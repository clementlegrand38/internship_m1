\documentclass{llncs}

\usepackage[utf8]{inputenc}%
\usepackage[main=english]{babel}% % Adjust the main language

\usepackage{todonotes}
\usepackage{amsfonts,amsmath,amssymb}%
\usepackage{xspace}%
%\usepackage{subfig}
\usepackage{subcaption}
\usepackage{graphicx}
%\usepackage{newpxtext, newpxmath}
%\usepackage{array}
%\usepackage{algorithm2e}


\newcommand{\Rm}{Reidemeister move\xspace}
\newcommand{\Rms}{Reidemeister moves\xspace}

\usepackage{amsthm}%

\newtheorem{thm}{Theorem}[section]%
\newtheorem{cor}[thm]{Corollary}%
\newtheorem{lem}[thm]{Lemma}%

\theoremstyle{proposition}%
\newtheorem{proposition}[thm]{Proposition}%

\theoremstyle{definition}%
\newtheorem{definition}[thm]{Definition}%

\theoremstyle{remark}%
\newtheorem*{remark}{Remark}%

\theoremstyle{example}%
\newtheorem*{example}{Example}%

\renewcommand\qedsymbol{$\blacksquare$}%
\DeclareMathOperator{\dfct}{def}
\DeclareMathOperator{\cross}{cross}

\begin{document}


\title{Parametrized complexity of a knot theory problem}

\author{Clément Legrand-Duchesne}

\institute{Univ Rennes, F-35000 Rennes, France}
\maketitle

\noindent{\small{\emph{Internship carried out in Charles University in
      Prague, from June 3 to August 11, 2019,  under the supervision of Martin
      Tancer.}}}
\begin{abstract}
    Abstract is here.

\keywords{Knot theory \and Parametrized complexity \and Reidemeister
  moves \and Untangling \and Defect.}
\end{abstract}



\section{Introduction}\label{sec:Introduction}
A knot can be thought of as a piece of string whose ends are attached
together. One of the most famous and important problems in knot theory
consists in ascertaining whether or not two knots are equivalent, that
is if we can distort the first one into the second one without
breaking it. A simpler problem is to determine if a knot is equivalent
to the trivial knot, an unknotted circle. Haken presented an algorithm
to solve this problem called the unknot recognition problem. However,
this algorithm is very complicated and time-consuming. It has been
proven that the unknot recognition problem is in \emph{NP} and in
co-\emph{NP}, but as of today, we do not know if it belongs to $P$.

To know if a knot is equivalent to the unknot, a natural idea consists
in projecting the knot on a plane to obtain a diagram, and try to
perform some local changes to progressively remove the crossings and
untangle the diagram. One way to do this is to use \Rms, that are a
collection of local changes on a diagram that do not change the knot
equivalence class of the diagram. Lackenby proved that for any diagram
there exists a untangling sequence of \Rms of length polynomial in the
number of crossings of the diagram \cite{lackenby}. Unfortunately,
there are diagrams for which any untangling sequence has to increase
the number of crossings at some point \cite{culprit}. These diagrams
are called ``hard unknots'' and prevent us from having a greedy
strategy. In fact, the problem of finding the shortest sequence of
\Rms to untangle a diagram is called the untangling problem and is
\emph{NP}-complete \cite{tancer}.

The \emph{NP}-completeness proof provided in \cite{tancer} consists in
two steps. The first one introduces the notion of \emph{defect} of a
diagram, that depends on the minimum number of moves of
any untangling sequence and on the number of crossings of the
diagram. Then it proves that the problem of determining the defect of
a diagram is \emph{NP}-complete. The second step proves the
\emph{NP}-completeness of the untangling problem, by using the relationship between the
defect and the minimal number of moves of an untangling sequence.

Thus, it seems that the difficulty of the untangling problem is
tightly tied to the defect of the diagram. This paper focuses on the
complexity of the untangling problem with respect not only to the size
of the diagram but also to its defect. We show that the untangling
problem parametrized by the defect belongs to the \emph{XP} class, by giving an
algorithm running in $O(n^{3d})$ where $n$ is the size of the diagram
and $d$ its defect. We then show that this parametrized problem is
\emph{W[P]}-hard by reducing it to the minimum axiom set problem.

This paper is organized as follows, first we introduce prerequisites
related to knot theory and parametrized complexity. In Section
\ref{sec:XP} and Section \ref{sec:hardness} we give the main ideas of
the proofs of the belonging to \emph{XP} and the $W[P]$-hardness
of the untangling problem parametrized by the defect.

\section{Prerequisite}\label{sec:Prerequisites}
\subsection{Knot theory}
A \emph{knot} is an embedding of the circle in
$\mathbb{R}^3$, that is, a continous injectiv map $K : S^1 \to
\mathbb{R}^3$. A continous bijection $h : \mathbb{R}^3 \to
\mathbb{R}^3$ that has a continous inverse function is called an
\emph{homeomorphism}. Two knots $K_1$ and $K_2$ are \emph{equivalent} if
there exists a family of homeomorphisms $(F_t : \mathbb{R}^3 \to
\mathbb{R}^3)_{t \in [0,1]}$ such that
\begin{itemize}
\item $F_0$ is the identity map,
\item the function $t \mapsto F_t$ is continous,
\item $K_2 = F_1 \circ K_1$.
\end{itemize}
A family of homeomorphisms verifying this property is called an
\emph{ambiant isotopy} taking $K_1$ to $K_2$ and formalizes the idea
of continous distorsion of the space. The canonical injection of $S^1$
in $\mathbb{R}^3$ is called the \emph{unknot} or \emph{trivial knot}.

A \emph{diagram} of a knot is an embedding $D$ of $S^1$ in
$\mathbb{R}^2$ such that every point of the plane has at most two
preimages and only a finite number of them have a exactly two
preimages. The points having two preimages are called \emph{crossings}. The
diagram also contains the information of which arc passes over the
other at each crossing, when we represent a diagram, this is done by
interrupting the arc that passes under in the neighborhood of the
crossing. A diagram can also be seen as the composition of a knot with
a projection that stores the information of the over and underpasses
and such that the arcs cross tranversally and a finite number of
times. The image by $D$ of a closed interval $[a,b]$ viewed as a
subset of $S^1$ such that $D(a)$ and $D(b)$ are crossings is called an
\emph{arc} and will be denoted by $(D(a)D(b))$. We identify diagrams
with there representation in $\mathbb{R}^2$.

The \Rms consists in a collection of local changes on diagrams that
preserv the isotopy class of the corresponding knot. There are three
types of \Rms, the first one twists or untwists an arc (Figure
\ref{fig:RM1}), the second one passes an arc under another to create
or remove a bigon (Figure \ref{fig:RM2}), the third one passes a arc
completely under or over a crossing (Figure \ref{fig:RM3}). The red
circle delimiting the area of the knot affected by the move will be
used later. Type $I$ and $II$ \Rms change the number of crossings of
the diagram, so we distinguish type $I^+$ that creates a new
crossings from type $I^-$ that deletes a crossings, and likewise for
type $II$ moves.

Alexander and Briggs showed that for every two diagrams
$D_1$, $D_2$ of a same knot, one can go from $D_1$ to $D_2$ with a
sequence of \Rms %\todo{cite}.
. Östlund, Manturov and Hagge showed that
for every knot $K$, there exists two diagrams $D_1$ and $D_2$ of $K$,
such that every sequence of \Rms leading from $D_1$ to $D_2$ contains
the three different types of moves %\todo{cite}.

\begin{figure}[h!]
  \begin{subfigure}[b]{0.5\textwidth}
    \centering  
    \includegraphics[width=1\textwidth]{../figures/pdf/RMI.pdf}
    \caption{Type $I$ \Rms}
    \label{fig:RM1}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.5\textwidth}
    \centering  
    \includegraphics[width=1\textwidth]{../figures/pdf/RMII.pdf}
    \caption{Type $II$ \Rms}
    \label{fig:RM2}
  \end{subfigure}
  \begin{center}
  \begin{subfigure}[b]{0.5\textwidth}
    \centering  
    \includegraphics[width=1\textwidth]{../figures/pdf/RMIII.pdf}
    \caption{Type $III$ \Rm}
    \label{fig:RM3}
  \end{subfigure}
  \end{center}
  \caption{The different \Rms}
  \label{fig:RM}
\end{figure}

Let $D$ be a diagram of an unknot, we say that a sequence of \Rms
untangles $D$ if performing $S$ on $D$ results in a diagram with no
crossings.

The \emph{defect} $\dfct(S)$ of an untangling sequence $S$ is defined
as two time the length of $S$ minus the number of crossings in $D$. We
define the defect of a diagram to be the defect of the shortest
untangling sequence of $D$.

Since a \Rm removes at most two crossings, the defect of a sequence is
always non-negative. Moreover, the type $II^-$ move is the only \Rm
removing exactly two crossings, thus $\dfct(S) = 0$ if and only if $S$
is composed of type $II^-$ \Rms only. Likewise, it is possible to
distribute the contribution of each type of move to the defect of the
sequence by counting the number of moves it adds or remove to the
diagram: a type $II^-$ \Rm contributes for 0, a type $I^-$ \Rms deletes one
crossing and thus contributes for 1, a type $III$ \Rms contributes for 2,
and finally type $I^+$ and $II^+$ contribute for 3 and 4 respectively.

\subsection{Parametrized complexity}
In classical computational complexity theory, the problems are
classified according to their difficulty with respect to the size of
the instance only. As of today, all the deterministic algorithms that
we know of for NP-complete problems run in superpolynomial time.

However, if we measure the time complexity not only in respect to the
size of the instance but also with respect to an additional parameter,
the problem might become much easier for the instances for which the
parameter is small, even though the size of the instance is big. More
precisely, if we denote by $n$ the size of the instance, there are
many NP-complete problems for which there exists an additional
parameter $k$ and an deterministic algorithm running in $O(P(n)f(k))$
where $P$ is a polynomial and $f$ is an arbitrary computable function
in $k$. Such problems are called \emph{fixed-parameter tractable} and
parametrized complexity is the study of the time complexity of
problems with respect to the size of the input and to a parameter.

The class of fixed-parameter tractable problems is called \emph{FPT}. The \emph{SAT}
problem parametrized by the number of variables is fixed-parameter
tractable and the vertex cover problem parametrized by the size of the
cover as well \todo{cite}.

There are other parametrized complexity classes, the $W$-hierarchy is
a collection of classes $(W[i])_{i \ge 0}$ such that $\text{\emph{FPT}} = W[0]$ and
$W[i] \subseteq W[i+1]$. The defintion of the classes $W[i]$ uses
combinatorial circuits, is technical and of no real interest here.
We will only be using the class $W[P]$ that is the union of all
classes $W[i]$. The class $W[P]$ can also be defined as the class of
parametrized problems that can be decided in time $O(P(n)f(k))$ by a
non-deterministic Turing machine using at most $h(k)\log(n)$
non-deterministic steps, where $f$ and $h$ are arbitrary computable
functions and $P$ is a polynomial.

Finally the \emph{XP} class is composed of the parametrized problems
that can be solved by a deterministic algorithm running in time
$O(n^{f(k)})$ where $f$ is an arbitrary computable function. These
problems are said to be slicewise polynomial: for every $k$, the
problems restricted to the instances of parameter at most $k$ can be
solved in polynomial time. The \emph{XP} class contains the
$W$-hierarchy (see Figure \ref{fig:param}) and the inclusion
$\text{\emph{FPT}} \subset \text{\emph{XP}}$ is known to be strict.

\begin{figure}
  \centering
  \includegraphics[width=.8\textwidth]{../figures/pdf/parametrized_complexity.pdf}
  \caption{Inclusions of the parametrized complexity classes}
  \label{fig:param}
\end{figure}

The notion of reduction can be defined in paramerized complexity as
well: a \emph{fpt}-reduction from a parametrized problem $(A, \kappa_A)$
to a parametrized problem $(B, \kappa_B)$ where $\kappa_A$ and $\kappa_B$
are the parameters, is a function $R : x \mapsto R(x)$ such that $x
\in A$ if and only if $R(x) \in B$, it is possible to compute $R$ with
an \emph{FPT}-algorithm with respect to $\kappa_a$ and there exists a
computable function $g$ such that $\kappa_b(R(x)) \le g(\kappa_a(x))$ for
all instance $x$. The aformentionned classes are close under
\emph{fpt}-reductions.

\section{Belonging to \emph{XP}}\label{sec:XP}

The aim of this section is to present a greedy algorithm solving the
following parametrized problem in time $O(n^{3d})$:\\
\textsc{Input:} a diagram $D$ with $n$ crossings,\\
\textsc{Parameter:} an integer $d$, \\
\textsc{Question:} Is there a sequence $S$ of defect $d$ that
untangles $D$ ?

The main intuition is that type $II^-$ \Rms can be performed greedily
because the combinatorics and difficulty of the problem comes from the
moves that contribute to the defect, namely all types of move but
$II^-$. For instance, one can show that for a diagram of defect 0,
that means that it can be untangled with type $II^-$ moves only, it is
possible to proceed greedily by performing at each step any feasible
type $II^-$ move without blocking ourselves at some point.

\subsection{Preliminary results on the feasibility of \Rms}

Let $D$ be a diagram of an unknot and $c_1, \dots c_N$ its
crossings. In the following, we will write
\begin{itemize}
\item $\{c_i\}_{I^-}$ to refer to the type $I^-$ \Rm removing $c_i$,
  if feasible.
\item $\{c_i, c_j\}_{II^-}$ to refer to the type $II^-$ \Rm removing
  $c_i$ and $c_j$, if feasible
\item $\{c_i, c_j, c_k\}_{III}$ to refer to the type $III$ \Rm
  affecting $c_i$, $c_j$ and $c_k$, if feasible.
\end{itemize} 

The \emph{faces} of a plane graph are the areas delimited by the
edges. We will here consider that the faces are closed (they contain
their incident edges and vertices). By a slight abuse of terminology,
we will also call face the area of a diagram that is a face of the
underlying plane graph. The inner faces are the compact faces of the
diagram and the outer face refers to the only non-compact face of the
diagram.\\ %% Any face other than the outer face is simply connected if
%% and only if it is not incident to a loop edge. The interior of any
%% face other than the outer face is simply connected. All the faces are
%% connected.

%% Let $D_1$ be a diagram and $m$ be an arbitrary \Rms on $D_1$. Let
%% $D_2$ be the diagram obtained from $D_1$ after $m$. The \Rms are local
%% changes, thus there exists a disk $\mathcal{D}$ that contains only the
%% arcs and crossings involved in $m$ (see Figure \ref{fig:RM}). Let
%% $\alpha_1$ be an arc of $D_1$, we want to keep track of $\alpha_1$
%% after the modifications induced by $m$, for that we will identify
%% $\alpha_1$ to an arc $\alpha_2$ of $D_2$ and say that $\alpha_2$ is
%% \emph{descended from} $\alpha_1$.

%% There are several possibilities,
%% \begin{itemize}
%% \item $\alpha_1$ does not intersect $\mathcal{D}$, then
%%   $\alpha_1$ is unmodified by $m$ and we set $\alpha_2 = \alpha_1$,
%% \item $\alpha_1$ enters and comes out of $\mathcal{D}$, none of the
%%   endpoints is in $\mathcal{D}$, we set $\alpha_2$ to be the arc
%%   with same endpoints that is obtained from $\alpha_1$ when
%%   performing $m$,
%% \item $\alpha_1$ has exactly one endpoint $c$ in $\mathcal{D}$, then we
%%   set $\alpha_1$ to be the arc with same endpoints in $D_2$,
%% \item $\alpha_1$ is entirely included in $\mathcal{D}$, if $m$ is of
%%   type $I^-$ or $II^-$, then $\alpha_1$ is just removed and there no
%%   arc $\alpha_2$ descended from $\alpha_1$; if $m$ is
%%   of type $III$, we set $\alpha_2$ to be the arc included in
%%   $\mathcal{D}$ with same endpoints as $\alpha_1$.
%% \end{itemize}

%% We also want to do the same thing with the faces of both diagrams,
%% let $F_1$ be a face of $D_1$, we identify $F_1$ with a face of $D_2$
%% and say that $F_2$ is descended from $F_1$:
%% \begin{itemize}
%% \item if $F_1$ does not intersect with $\mathcal{D}$, $F_1$ is
%%   unmodified by $m$ and we set $F_2 = F_1$ in $D_2$,
%% \item if $F_1$ intersects the boundery of $\mathcal{D}$, we set
%%   $F_2$ to be the face of $D_2$ that intersects the subset of the
%%   boundery of $\mathcal{D}$ that $F_1$ intersects. Note that if $m$
%%   is of type $II^-$ and $F_1$ traverses $D$, than no such face of $D_2$
%%   exists but there are two faces $F_2$ and $F_2'$ of $D_2$ such that
%%   $F_2 \cup F_2'$ intersects the same subset of the boundery of
%%   $\mathcal{D}$ as $F_1$ and we say that $F_1$ is split into $F_2$
%%   and $F_2'$;
%% \item if $F_1$ is entirely contained in $\mathcal{D}$, if $m$ is of
%%   type $I^-$ or $II^-$ then $F_1$ is simply removed and there is no
%%   face descended from $F_1$; if $m$ is of type $III$, we set $F_2$
%%   to be the face of $D_2$ incident with the crossings that were moved.
%% \end{itemize}


Let $G$ be the underlying plane graph of $D$. Let $A$ be a set of
disjoint arcs of $D$ such that the subgraph $G'$ induced by the edges
forming the arcs of $A$ is eulerian. Each face $F'$ of $G'$
corresponds to a set of faces $\{F_1, \dots F_n\}$ of $G$ such that $F' =
\bigcup F_i$. Let $\mathcal{S}$ be the set of faces of $G$ that are
included in an inner face of $G'$. We will call $\mathcal{S}$ the
\emph{set of the faces spanned by} $A$.

%% \begin{lem}
%%   A face $F$ of $D$ belongs to the set of faces spanned by $A$ if and
%%   only if for any sequence of faces $(F_1, \dots F_n)$ such that for
%%   all $i$, $F_i$ is adjacent to $F_{i+1}$ and $F_1 = F$ and $F_n$ is
%%   the outer face; there exists $i$ such that one of the arcs of $A$
%%   separates $F_i$ from $F_{i+1}$.
%%   \begin{proof}
%%     Let $\mathcal{S}$ be the set of faces spanned by $A$ and $F \in
%%     \mathcal{S}$. Let $(F_1, \dots F_n)$ be a sequence verifying these
%%     three properties. Since $F_1$ belongs to $\mathcal{S}$ and $F_n$
%%     does not, there exists $i$ such that $F_i \in \mathcal{S}$ and
%%     $F_{i+1} \notin \mathcal{S}$, thus $F_i$ is included in one of the
%%     inner faces of $G'$ and $F_{i+1}$ is included in the outer face of
%%     $G'$ and $F_i$ is separated from $F_{i+1}$ by an arc of $A$.

%%     Regarding the other direction of the equivalence, suppose that
%%     there exists a sequence $(F_1, \dots F_n)$ of adjacent faces
%%     leading from $F = F_1$ to $F_n$ the outer surface, such that for
%%     every $i$, $F_i$ and $F_{i+1}$ are not separated by an arc of
%%     $A$. Thus, $F_i$ and $F_{i+1}$ are included in the same face of
%%     $G'$ and by transitivity, $F$ is included in the outer face of
%%     $G'$.
%%   \end{proof}
%% \end{lem}

%% \begin{lem}
%%   \label{coloring}
%%   Let $G_1$ be the underlying graph of a diagram $D_1$ of an
%%   unknot. Let $A_1$ be a set of disjoint arcs of $D_1$ such that the
%%   subgraph $G_1'$ induced by the edges forming the arcs of $A_1$ is
%%   eulerian. Since $G_1$ is 4-regular, every vertex of $G_1'$ has
%%   degree 2 or 4. Thus there exists a coloring of the faces of $G_1'$
%%   using only two colors, that is, a map from the set of faces of
%%   $G_1'$ to $\{0,1\}$ such that any two adjacent faces have different
%%   colors.\\ By convention, we will set the color of the outer face to
%%   0. We assign to each face $F$ of $G_1$ a color equal to the color of
%%   the face of $G_1'$ that includes $F$ (notice that two adjacent faces
%%   of $G_1$ might have the same color, so this is not a coloring of the
%%   faces of $G_1$).\\ Let $c$ be a crossing of $D_1$, let us perform on
%%   $D_1$ a \Rm $m$ that does not affect $c$. Let $D_2$ be the diagram
%%   obtained, $G_2$ its underlying fraph and $A_2$ the set of arcs
%%   descended from the arcs of $A_1$. The neighborhood of $c$ is not
%%   affected by the \Rm, thus we can label each of the four areas
%%   delimited by the knot in the neihborhood of $c$. For each of this
%%   labeled area, the coloring of the face it is included in is
%%   identical before and after the move.
%%   \begin{proof}
%%     The set $A_2$ induces a eulerian subgraph of $G_2$. Indeed, one
%%     can easily verify from Definition \ref{descended} than every
%%     unremoved crossing has as many incident arcs form $A_1$ in $D_1$
%%     than from $A_2$ in $D_2$, thus, the coloring in $D_2$ is well
%%     defined.
    
%%     For each type of move, it is possible to restrict the changes made
%%     on the diagram by $m$ to a disk $\mathcal{D}$ that contains only
%%     the arcs and crossings involved in $m$, this disk is delimited by
%%     the red circle on Figure \ref{fig:RM}. Thus, the only faces of
%%     $D_1$ that are affectd by the move are the faces that intersect
%%     with $\mathcal{D}$, the other faces are not modified.
    
%%     Let us color the faces of $D_2$ that were not affected by the
%%     move. By convention, the color of the outer face is equal to
%%     0. Let $F$ be an inner face of $D_1$ that is not entirely included
%%     in $D$. We claim that there exists a sequence of unmodified faces
%%     $(F_0, \dots F_n)$ of $D_2$ such that $F_0$ is the outer face,
%%     $F_n$ is adjacent to $F$ and every two consecutive faces $F_i$ and
%%     $F_{i+1}$ are adjacent (we will prove this allegation
%%     later). Since the faces $F_i$ are unmodified by the move, the
%%     sequence $(F_0, \dots F_n)$ verifies the same properties in
%%     $D_1$. Moreover, the arcs incident to the unmodified faces are
%%     unmodified as well, thus $F_i$ and $F_{i+1}$ have different color
%%     in $D_1$ if and only if they have different color in $D_2$. This
%%     proves that $F$ has identical color in $D_1$ and $D_2$.

%%     The crossing $c$ is not affected by the move $m$ and so can not be
%%     incident to a face entirely included in $\mathcal{D}$.

%%     Concerning the existance of this sequence of faces, $F$ is
%%     adjacent to an unmodified face $F_n$ and the union of the
%%     unmodified faces forms a connected subset of $\mathbb{R}^2$, thus
%%     there is a generic path starting in $F_n$ and ending in the outer
%%     face $F_0$ and all the faces $F_i$ it traverses are consecutively
%%     adjacent.
%%   \end{proof}
%% \end{lem}

\subsubsection{Feasibility of type $I^-$, $II^-$ and $III$ \Rms}

We will here focus on type $II^-$ \Rms, the definitions and lemmas can
be extended to 
\begin{definition}
  \label{2closeness}
  Let $C$ be a subset of crossings of $D$, $k$ a non-negative integer
  and $c_i$, $c_j$ two crossings of $D$.  We say that $c_i$ and $c_j$
  are $k$-close with respect to $C$ (see \cite{tancer}) if there exists
  two arcs $\alpha$ and $\beta$ connecting $c_i$ and $c_j$ such that:
  \begin{itemize}
  \item $\alpha$ enters $c_i$ and $c_j$ both as an overpass,
  \item $\beta$ enters $c_i$ and $c_j$ both as an underpass,
  \item $\alpha$ and $\beta$ may have self crossings in their
    interiors, but neither $c_i$ nor $c_j$ lies in their interiors
    (see figure \ref{fig:kclose1} and \ref{fig:kclose2}),
  \item $\alpha$ and $\beta$ together contain at most $k$ crossings
    from $C$ in their interiors. The self-crossings and the crossings
    that are in the interior of both $\alpha$ and $\beta$ are counted
    only once.
  \end{itemize}

  \begin{figure}[h!]
    \begin{subfigure}[b]{0.4\textwidth}
      \centering  
      \includegraphics[width=.4\textwidth]{../figures/pdf/kclose2.pdf}
      \caption{$c_i$ and $c_j$ are 3-close in respect to $\{a, c, d\}$}
      \label{fig:kclose1}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.4\textwidth}
      \centering  
      \includegraphics[width=.4\textwidth]{../figures/pdf/kclose1.pdf}
      \caption{The choice of $\alpha$ does not respect the third
        condition of the $k$-closeness definition}
      \label{fig:kclose2}
    \end{subfigure}

    \caption{Examples illustrating the $k$-closeness property}
    \label{fig:kclose}
  \end{figure}

\end{definition}

\begin{lem}
  \label{feasible2-}
  $\{c_i, c_j\}_{II^-}$ is feasible if and only if
  \begin{enumerate}
    \item\label{cond1} $c_i$ and $c_j$ are 0-close with respect to all
      the other crossings of the diagram
    \item\label{cond2} The set $\mathcal{S}$ of faces spanned by
      $\{\alpha, \beta\}$ is composed of a single face. This face is
      incident to $c_i$, $c_j$ and to the arcs $\alpha$ and $\beta$
      from Definition \ref{2closeness}.
  \end{enumerate}
  \begin{proof}
    The verification of the two conditions assuming that $\{c_i,
    c_j\}_{I^-}$ is feasible is straightforward.\\ Regarding the
    backward implication, let us assume the two conditions. The arcs
    $\alpha$ and $\beta$ do not contain any crossings in their
    interiors thus they each correspond to one edge of the underlying
    graph $G$ of $D$. Let $F$ be the face of $G$ spanned by $\alpha$
    and $\beta$, $F$ is simply connected. Thus, $F$ is a disk and
    $\alpha$, $\beta$ form a bigon that can be removed with a type
    $II^-$ move as depicted on figure \ref{fig:0close1}.
    
    The figure \ref{fig:0close} shows different configurations in
    which $c_i$ and $c_j$ are 0-close to emphasize the importance of
    the second condition to make $\{c_i, c_j\}_{I^-}$ feasible (the
    green area is the union of the faces of $\mathcal{S}$).
    
    \begin{figure}[h]
      \begin{subfigure}[b]{0.3\textwidth}
        \centering  
        \includegraphics[width=\textwidth]{../figures/pdf/knot1.pdf}
        \caption{The only configuration verifying the two conditions
          of Lemma \ref{feasible2-}}
        \label{fig:0close1}
      \end{subfigure}
      \hfill
      \begin{subfigure}[b]{0.3\textwidth}
        \centering  
        \includegraphics[width=\textwidth]{../figures/pdf/knot2.pdf}
        \caption{One configuration verifying the 0-closeness, but not the
          second condition of Lemma \ref{feasible2-}}
        \label{fig:0close2}
      \end{subfigure}
      \hfill
      \begin{subfigure}[b]{0.3\textwidth}
        \centering  
        \includegraphics[width=\textwidth]{../figures/pdf/knot3.pdf}
        \caption{One configuration verifying the 0-closeness, but not the
          second condition of Lemma \ref{feasible2-}}
        \label{fig:Oclose3}
      \end{subfigure}

      \caption{Outline of the different knots in which $c_i$ and $c_j$
        are 0-close.}
      \label{fig:0close}
    \end{figure}
  \end{proof}
\end{lem}

\begin{lem}
  \label{2closeresist}
  Let $D$ be a diagram of a knot and $c_i$, $c_j$ be two crossings of
  $D$. Suppose that $c_i$ and $c_j$ are $k$-close in $D$ with respect
  to a certain set of crossings $C$, and $\alpha$, $\beta$ be the two
  arcs from Definition \ref{2closeness}. Let $D'$ be the diagram
  obtained from $D$ after an arbitrary sequence $S$ of \Rms that do
  not involve $c_i$ or $c_j$. The arcs $\alpha'$ and $\beta'$
  descended from $\alpha$ and $\beta$ by applying $S$ verify the first
  three conditions of Definition \ref{2closeness} and the crossings
  $c_i$ and $c_j$ remain $k$-close with respect to the subset of
  crossings of $C$ that were preserved.
  \begin{proof}
    Let us perform in $D$ one \Rm that does not involve $c_i$ or
    $c_j$. It is straightforward that the first three properties
    required on $\alpha'$ and $\beta'$ are verified.

    The last property is also verified: in the case of type $I^+$ or
    $II^+$ move, the new crossings do not belong to $C$ and the subset
    of crossings of $C$ that were in the interior of $\alpha$ and
    $\beta$ stays identical. In the case of a type $I^-$ or $II^-$
    \Rm, one or several crossings are removed and the other crossings
    are left untouched, therefore, there cannot exist any crossing in
    the interior of $\alpha'$ and $\beta'$ after the move that was not
    here before. In the case of a type $III$ move, one may notice that
    $\alpha'$ and $\beta'$ contain an unchanged set of crossings since
    neither $c_i$ nor $c_j$ is involved.
  \end{proof}
\end{lem}

\begin{lem}
  \label{resistance2-}
  Let $D_1$ be a diagram of a knot and $c_i$, $c_j$ be two crossings of
  $D_1$ such that $\{c_i, c_j\}_{II^-}$ is feasible, let $\alpha$ and
  $\beta$ be the arcs connecting $c_i$ to $c_j$ from Definition
  \ref{2closeness}. Let us perform on $D_1$ a sequence $S$ of \Rms that
  do not involve $c_i$ and $c_j$. Let $D_2$ be the diagram obtained
  from $D_1$ after $S$.\\ If the arcs $\alpha_2$ and $\beta_2$ descended
  from $\alpha_1$ and $\beta_1$ contain no crossings of $D_2$, then $c_i$
  is 0-close to $c_j$ with respect to all the other crossings of $D_2$
  and $\{c_i, c_j\}_{II^-}$ is feasible in $D_2$.
  \begin{proof}
    A proof of this lemma can be found in appendix \todo{proof}.
  %%   Let $G_1$ be the underlying graph of $D_1$ and $G_1'$ be the
  %%   subgraph of $G_1$ induced by $\alpha_1$ and $\beta_1$. Likewise we
  %%   define $G_2$ and $G_2'$.

  %%   There is only face $F_1$ of $G_1$ spanned by $\{\alpha_1,
  %%   \beta_1\}$, thus $F_1$ is a face of $G_1'$ as well and is its only
  %%   inner face. The face $F_1$ is incident to $\alpha_1$, $\beta_1$, $c_i$
  %%   and $c_j$. Let us color $G_1$ as described in Lemma
  %%   \ref{coloring}, $F_1$ has color 1. The neighborhoods of the
  %%   crossings $c_i$ and $c_j$ are not affected by $S$, we will also
  %%   label the area in the neighborhood of $c_i$ that belongs to
  %%   $F_1$. The other faces incident to the neighborhood of $c_i$ have
  %%   color 0.

  %%   Let $F_2$ be the face of $G_2$ incident to $c_i$, that contains
  %%   the labeled area. This face is incident to $\alpha_2$ and
  %%   $\beta_2$. Since $\alpha_2$ and $\beta_2$ contain no crossings,
  %%   $c_i$ and $c_j$ are 0-close in regard to all the other crossings
  %%   of $D_2$ and $F_2$ is also incident to $c_j$. Lemma \ref{coloring}
  %%   shows that $F_2$ has color 1 and not 0, thus $F_2$ is included in
  %%   the inner face of $G_2'$. The same reasonning can be applied to
  %%   the other areas in the neighborhood of $c_i$ to prove that the
  %%   faces they belong to in $D_2$ have color 0.

  %%   Thus $F_2$ is also a face of $G_2'$ and $G_2'$ is composed of
  %%   only
  %%   two faces, separated by the bigon formed by $\alpha_2$ and
  %%   $\beta_2$. Since $c_i$ and $c_j$ are 0-close with respect to all
  %%   the other crossings of $D_2$ and $F_2$ is the only face of $G_2$
  %%   spanned by $\{\alpha_2, \beta_2\}$, Lemma \ref{feasible2-} can be
  %%   applied and $\{c_i, c_j\}_{II^-}$ is feasible in $D_2$.
  \end{proof}
\end{lem}

  The definition of $k$-closeness can be extended to singletons and
  triplets of crossings and the lemmas can be adapted to the
  feasibility of type $I^-$ and $III$ \Rms.

\section{\emph{W[P]}-hardness}\label{sec:hardness}
blabla

\section{Conclusion}\label{sec:Conclusion}

Conclusions are here.

\section*{Acknowledgments}\label{sec:Acknowledgments}

Authors would like to thank YYYYY.

\nocite{*}
\bibliographystyle{splncs04}
\bibliography{../biblio}

\end{document}
