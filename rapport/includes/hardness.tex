\section{$W[P]$-hardness}

\todo[inline]{THIS SECTION IS WORK IN PROGRESS}

The aim of this section is to prove that the problem of finding the
shortest untangling sequence knowing the defect of a diagram is
$W[P]$-hard. For this we reduce it to the minimum axiom set problem
which is known to be $W[P]$-hard:\\
\underline{\textsc{Input:}} a set of statements $\mathcal{S} = \{s_1,
\dots s_n\}$ and a set $\mathcal{R}$ of couples $(\mathcal{T}_i, t_i)$
where $\mathcal{T}_i \subset \mathcal{S}$ and $t_i \in
\mathcal{S}$.\\
\underline{\textsc{Parameter:}} an integer $K$.\\
\underline{\textsc{Question:}} does there exist $\mathcal{S}_0 \subset
\mathcal{S}$ of size $K$ and a positive integer $n$ such that if we
define $\mathcal{S}_i$ to be the subset of statements $t_k$ that
belong to $\mathcal{S}_{i-1}$ or for which there exist a couple
$(\mathcal{T}_k, t_k)$ with $\mathcal{T}_k \subset \mathcal{S}_{i-1}$,
then $\mathcal{S}_n = \mathcal{S}$?\\

\subsection{Tools and notations}
The following is inspired by the work of A.de Mesmay, Y. Rieck,
A. Sedgwick and M. Tancer in \cite{tancer}, thus, for sake of
consistency, we will use in the following the same notations. This
subsection overviews the notions and notations introduced in \cite{tancer}

Let $D$ be a diagram of the unknot, we call untangling of $D$ any
sequence $\textbf{D} = (D^0, \dots D^k)$ such that $D^0 = D$ and $D^n$
is the trivial diagram and $D^i$ results from performing a single \Rm
on $D^{i-1}$. Given an untangling $\textbf{D} = (D^0, \dots D^k)$ of
$D$ and a crossing $r^i$ of $D^i$, the crossing $r^i$ can be removed
by the move applied to $D^i$ if it is of type $I^-$ or $II^-$. If $r^i$
does not vanish, we say that is \emph{survives} and denote by
$r^{i+1}$ the corresponding crossing in $D^{i+1}$.

\todo[inline]{rephrase} With a slight abuse of terminology, by a
crossing in D we mean a maximal sequence $\textbf{r} = (r^a , r^{a+1},
\dots r^b )$ such that $r^{i+1}$ is the crossing in $D^{i+1}$
corresponding to $r^i$ in $D^i$ for any $i \in [a, b-1]$. By
maximality we mean, that $r^b$ vanishes after the $(b + 1)$-st move
and either $a = 0$ or $r^a$ is introduced by the $a$-th Reidemeister
move (which must be a $I^+$ or $II^+$ move).  An initial crossing is a
crossing $\textbf{r} = (r^0 , r^1 , \dots r^b)$ in
$\textbf{D}$. Initial crossings in $\textbf{D}$ are in one-to-one
correspondence with crossings in $D = D^0$. For simplicity of
notation, $r^0$ is also denoted $r$ (as a crossing in $D$).  A
Reidemeister $II^-$ move in $\textbf{D}$ is \emph{economical}, if both
crossings removed by this move are initial crossings; otherwise, it is
\emph{wasteful}.  Let $m_3 (\text{bf})$ be the number of $III$ moves
affecting a crossing $\textbf{r}$. The weight of an initial crossing $\textbf{r}$
is defined in the following way:

\begin{align*}
  w(\textbf{r}) = \frac{2}{3}m_3(\textbf{r}) + \left\{
  \begin{array}{ll}
    0 & \text{if } \textbf{r} \text{ vanishes by an economical } II^-
    \text{move};\\
    1 & \text{if } \textbf{r} \text{ vanishes by a } I^-
    \text{move};\\
    2 & \text{if } \textbf{r} \text{ vanishes by a wasteful } II^- \text{move}.
  \end{array}
  \right.
\end{align*}

For later purposes, we also define $w(r) := w(\textbf{r})$ and $w(R)
:= \sum_{r \in R} w(r)$ for a subset $R$ of the set of all crossings in
$D$.

\begin{lem}
  Let $\textbf{D}$ be an untangling of a diagram $D$. Then
  $$def(\textbf{D}) \ge \sum_{\textbf{r}} w(\textbf{r}),$$ where the
  sum is over all initial crossings $\textbf{r}$ of $\textbf{D}$.
\end{lem}
\begin{proof}
  The proof uses the discharging technique and can be found in \cite{tancer}.
\end{proof}

\begin{lem}
  Let $\textbf{D}$ be an untangling of a digram $D$ which uses the
  $I^-$ and $II^-$ moves only. Then,
  $$def(\textbf{D}) = \sum_{\textbf{r}} w(\textbf{r}) = \text{number
    of type } I^- \text{ move},$$ where the sum is over all initial
  crossings $\textbf{r}$ of $\textbf{D}$.
\begin{proof}
  The proof can be found in \cite{tancer}.
\end{proof} 
\end{lem}

\todo[inline]{twin definition}

\begin{lem}
  Let $R$ be a subset of the set of crossings in $D$, let $c \in \{0,
  1, 2, 3\}$. Let $r$ be the crossing in $R$ which is the first of the
  crossings in $R$ removed by an economical $II^-$ move (we allow a
  draw). If $w(R) \le c$, then $r$ and its twin $t(r)$ are $c$-close
  neighbors with respect to $R$.
\begin{proof}
  The proof can be found in \cite{tancer}.
\end{proof} 
\end{lem}

\subsection{The construction}

Let $\mathcal{S}$ be a set of statements $s_1, \dots s_n$ and
$\mathcal{R}$ be a set of couples $(\mathcal{T}_j, t_j)$ where
$\mathcal{T}_j \subset \mathcal{S}$ and $t_i \in \mathcal{S}$.

The main idee is to have a long tentacle correspond to each statement,
such that the tentacle for a statement $s_i$ can be untangled and
pulled with only type $II^-$ if a type $I^-$ and is performed on it
before or if all the tentacles corresponding to a subset of statements
$\mathcal{T}_j \subset \mathcal{S}$ with $(\mathcal{T}_j, s_i) \in
\mathcal{R}$. This way, the defect of the untangling is equal to the
number of type $I^-$ moves performed, that is, to the number of
statements that we obtain by putting in the initial set
$\mathcal{S}_0$ and not thanks to a relation of $\mathcal{R}$.

A \emph{Link} is an embedding of a union of circles in $\mathbb{R}^3$,
concretely that means that a link is a composed of one or several
knots tangled together, each of this knots is called a
\emph{component} of the link. \emph{Brunnian links} are the links that
are not isotopic to a trivial link (a link in which the compoments are
independent trivial knots), but such that if any of the component is
removed, then the obtained link becomes trivial. In the following, we
will use \emph{Rubberband Brunnian links}, which are Brunnian links
with an arbitrary number of compoenents. One component of a brunnian
rubberband link is depicted Figure \ref{fig:bru_component}, all the
components are identical and form a sort of circular chain. For
instance, Figure \ref{fig:bru_4} shows the 4-component rubberband
Brunnian link. 

\begin{figure}[h]
  \centering  
  \includegraphics[width=.4\textwidth]{figures/pdf/brunnian_component.pdf}
  \caption{Rubberband brunnian link component}
  \label{fig:bru_component}
\end{figure}

\begin{figure}[h]
  \centering  
  \includegraphics[width=.6\textwidth]{figures/pdf/brunnian_4.pdf}
  \caption{The 4-component Rubberband brunnian link}
  \label{fig:bru_4}
\end{figure}

\paragraph{Step 1:}
Add the couple $(\{s_i\}, s_i)$ to $\mathcal{R}$ for every statement
$s_i$ that is not the second argument of any couple of $\mathcal{R}$.
This does not change the solution to the problem. We then duplicate
all the variables and all the couples: let $\mathcal{S}' = \{s_i' \ |
\ s_i \in \mathcal{S}\}$ and $\mathcal{R}' = \{(\{t_j' \ | \ t_j \in
\mathcal{T}_i\}, s_i') \ | \ (\mathcal{T}_j, s_j) \in
\mathcal{R}\}$. There exists a solution of size $k$ for
$(\mathcal{S},\mathcal{R})$ if and only if there exists a solution of
size $2k$ for
$(\mathcal{S}\cup\mathcal{S}',\mathcal{R}\cup\mathcal{R}')$.

Thus, in the following, we will assume that every statement is the
second argument of a couple and that the statements and couples are
duplicated and will denote $\mathcal{S} \cup \mathcal{S}'$
(respectively $\mathcal{R} \cup \mathcal{R}'$) by $\mathcal{S}$
(respectively $\mathcal{R}$).

\paragraph{Step 2:}
For each statement $s_i$ construct a Rubberband Brunnian Link with
$m_i+1$ components, where $m_i$ is the number of couples of
$\mathcal{R}$ of the form $(\mathcal{T}_j, t_j)$ with $t_j = s_i$. We
will denote by $C^k(s_i)$ the $k$-th component of the brunnian link
with $k \in [O, m_i]$. We cut open each of the components in one point
and double each strand, then place at near the endpoints of each
strand two points $u_l^k$ and $v_l^k$ as shown on Figure
\ref{fig:bru_4_doubled}. The arcs $(u_l^kv_l^k)$ will be used later to
connect the different components together.

\begin{figure}[h]
  \centering  
  \includegraphics[width=.8\textwidth]{figures/pdf/brunnian_4_doubled.pdf}
  \caption{Transformation of step 2 on the 4-component Rubberband brunnian link}
  \label{fig:bru_4_doubled}
\end{figure}

\paragraph{Step 3:} Let us describe the gadget that corresponds to
$\mathcal{S}$ and that will be used to connect the different brunnian
links together. For each couple of duplicated variable $(s_i, s_i')$,
we denote by $B(s_i)$ the diagram Figure \ref{fig:gadget_S}. The arcs
$\nu$ are used as junctions between these diagrams: for every $i \in
[1, n-1]$, we sum the knots $B(s_i)$ and $B(s_{i+1})$ along the arc
$\nu(s_i)$ and $\nu(s_{i+1}')$. We will denote the obtained gadget by
$B$.

\begin{figure}[h]
  \centering  
  \includegraphics[width=.8\textwidth]{figures/pdf/gadget_S.pdf}
  \caption{Gadget for the couple of statements $(s_i, s_i')$}
  \label{fig:gadget_S}
\end{figure}

\paragraph{Step 4:}
To each component $C^k(s_i)$ with $k >1 $, we assign a couple
$(\mathcal{T}_j, t_j)$ with $t_j = s_i$.

In every component $C^k(s_i)$ with $k>0$, we connect $u_3^k$ with
$u_4^k$ via a simple arc, and do the same in $C^k(s_i')$. In every
component $C^0(s_i)$, we connect $u_3^k$ with $u_4^k$ with an arc
doing a twist \todo{figure} such that the arc coming from $u_3^0(s_i)$
overpasses the one coming from $u_4^0(s_i)$, we also pass the arc
$(u_2^0(s_i) v_2^0(s_i))$ over $(u_1^0(s_i) v_1^0(s_i))$. We do the
same in $C^0(s_i')$ but with twists in the opposite direction.

For each statement $s_i$, we connect the components $C^k(s_i)$ for $k
\in [0, m_i-1]$ with the gadget $B$ with the piece of diagram depicted
Figure \ref{fig:connection}, that attaches itsef on the pendent endpoints
$w_1(s_i)$, $w_2(s_i)$ and $u_1^k(s_i)$, $u_2^k(s_i)$ for $k \in [0,
  m_i-1]$. Doing so, may not be possible without crossing other arcs
already in the construction, we take the convention that the added
arcs are all above the previously added arcs.

\begin{figure}[h]
  \centering  
  \includegraphics[width=.6\textwidth]{figures/pdf/connection.pdf}
  \caption{Connecting $B(s_i)$ with the components $C^k(s_i)$ where $k
    \in [0, m_i-1]$}
  \label{fig:connection}
\end{figure}

Finnally, we need to connect $C^{m_i}(s_i)$ with $B(s_i)$. To do that,
we connect $x_2(s_i)$ with $u_1^0(s_i)$ and $x_1(s_i)$ to
$(u_2^0(s_i)$ with two arcs $\gamma_1$ and $\gamma_2$ parrallel, next
to one another and such that for every gadget $C^k(s_j)$ that
corresponds to a couple $(\mathcal{T}_l, t_l)$ where $t_l = s_j$ and
$s_i \in \mathcal{T}_l$, the arcs $\gamma_1$ and $\gamma_2$ cross
$(u_1^k(s_j) \ v_1^k(s_j))$, $(u_2^k(s_j) \ v_2^k(s_j))$, $(u_3^k(s_j)
\ v_3^k(s_j))$ and $(u_4^k(s_j) \ v_4^k(s_j))$, respectively as un
overpass, an underpass, an underpass and an overpass. Beside these
specific arcs, $\gamma_1$ and $\gamma_1$ passes abose all the arcs
previously present in the construction.

\todo[inline]{exemple complet}

\begin{thm}
  Let $(\mathcal{S}, \mathcal{R}, K)$ be an instance of the minimal
  axiom set problem and $D$ be the diagram obtained with the
  preceeding construction. Then, $(\mathcal{S}, \mathcal{R}, k)$ has
  an axiom set $\mathcal{S}_0$ of size $K$ if and only if $D$ can be
  untangled with defect $2K$.
\end{thm}

The two following subsection prove each a direction of the
equivalence.

\subsection{Small axiom set implies small defect}

Let $\mathcal{S}_0$ be an axiom set of size $K$ for $(\mathcal{S},
\mathcal{R})$.

For every $s_i$ such that $s_i \in mathcal{S}^0$, we perform a type
$I^-$ move on $p_1(s_i)$. Now that $(u_3^0(s_i) \ u_4^0(s_i))$ is a
simple arc, with no crossings in its interior, we can use it to
perform type $II^-$ \Rms to remove completely the gadget $C^0(s_i)$.
The others gadgets $C^k(s_i)$ can then be remove one by one in
decreasing order as shown on Figure \todo{figure}. One can then
perform type $II^-$ \Rms with $v_2^k(s_i)$ and $v_3^k(s_i)$ until
obtaining a simple arc between $u_2^k(s_i)$ and $u_3^k(s_i)$, likewise
one can perform type $II^-$ \Rms to obtain a simple arc between
$u_1^k(s_i)$ and $u_4^k(s_i)$ for every $0 < k \le m_i$. Since
$u_3^k(s(i))$ and $u_3^k(s(i))$ were connected by a simple arc,
for every $0 < k \le m_i$, all the $u_l^k(s_i)$ are on a same arc that
contains no crossing.

Then, the arc between $x_1(s_i)$ and $x_2(s_i)$ can be pulled froms
the loops it goes through by successive type $II^-$ \Rms. At this
point, all the gadgets $B(s_i)$ of statements that belong to
$\mathcal{S}_0$ are of the form \ref{fig:gadgetS1}.

\begin{figure}[h]
  \centering  
  \includegraphics[width=.8\textwidth]{figures/pdf/gadget_S_1.pdf}
  \caption{The piece of diagram $\tilde{B}(s_i)$ obtained during the
    untangling of $B(s_i)$ if $s_i \in \mathcal{S}_0$}
  \label{fig:gadgetS1}
\end{figure}

\begin{figure}
  \centering  
  \includegraphics[width=.8\textwidth]{figures/pdf/gadget_S_2.pdf}
  \caption{The piece of diagram $\tilde{B}(s_i)$ obtained during the
    untangling of $B(s_i)$ if $s_i \notin \mathcal{S}_0$}
  \label{fig:gadgetS2}
\end{figure}

Let $\mathcal{S}_1$ be the next set of statements in the sequence of
the minimum axiom set problem. The set $\mathcal{S}_1 \setminus
\mathcal{S}_0$ is non empty otherwise the sequences of sets
$(\mathcal{S}_i)_i$ would be stationnary. Let $s_i$ be a statement of
$\mathcal{S}_1 \setminus \mathcal{S}_0$, there exists $(\mathcal{T}_k,
t_j)$ with $s_i = t_j$ such that $\mathcal{T}_k \subset
\mathcal{S}_0$. Let $C^k(s_i)$ be the gadget corresponding to
$(\mathcal{T}_k, t_j)$. The arc $(u_3^k(s_i) \ u_4^k(s_i))$ does not
contain any crossings anymore since those were removed by the type
$II^-$ pulling the arcs $(x_1(s_l) \ x_2(s_l))$ of the statements
belonging to $\mathcal{S}_0$.

Once again, the arc $(v_3^k(s_i) \ v_4^k(s_i))$ contains no crossings
and the arc $(v_1^k(s_i) \ v_2^k(s_i))$ can be pulled with type $II^-$
\Rms, thereby freeing the others components as described previously.
At this point the arc $(w_1(s_i) \ w_2(s_i))$ contains only two
crossings $p_1(s_i)$ and $p_2(s_i)$ that cancel each other in a type
$II^-$ \Rm.

By continuing so with $\mathcal{S}_2$ and so on and doing the same
operations with the gadgets of the duplicated statements, one obtains
the diagram corresponding to the sum along the arcs $\nu(s_i)$ and
$\nu(s{i+1}')$ of the pieces of diagrams $\tilde{B}(s_i)$ where
$\tilde{B}(s_i)$ is the diagram of Figure \ref{fig:gadgetS1} if $s_i \in
\mathcal{S}_0$ and is the diagram of Figure \ref{fig:gadgetS2} otherwise.

We can untangled this diagram with only type $II^-$ \Rms by removing
the crossings two by two, starting from the left : if $s_n \in
\mathcal{S}_0$, then $p_2(s_n)$ remains and can be removed along with
$p_3(s_n)$ and likewise $p_2(s_n')$ and $p_3(s_n')$ cancel each other out in
a type $II^-$ move; if on the contrary $s_n \notin \mathcal{S}_0$,
then neither $p_2(s_n)$ neither $p_2(s_n')$ remain and the crossings
$p_3(s_n)$ and $p_3(s_n')$ can be removed by a type $II^-$ move.

The untangling sequence we describe is composed of $2|\mathcal{S}^0|$
type $I^-$ \Rms and only of $II^-$ otherwise, thus $D$ can be untagled
with defect $2K$. 

\subsection{Small defect implies small axiom set}

Let \textbf{D} be an untangling of $D$ with defect $\dfct(\textbf{D})
\le 2K$.  In each gadget $C^k(s_i)$, we label the crossings in the
intersection of $C^k(s_i)$ with $C^{k+1}(s_i)$, where $k$ is
considered modulo $m_i+1$, by grouping them four by four (see Figure
\ref{fig:grouping_crossing}). In each group labeled by $z \in
\{a,b,c,d,e,f,g,h\}$ we label the upper left crossing by $z_1(k)$, the
lower left by $z_2(k)$, the upper right by $z_3(k)$ and the lower
right by $z_4(k)$ (see Figure \ref{fig:labela} and \ref{fig:labelc}).
To ease the reading, we will only write $z_l$ when there is no
ambiguity on which component $z(l)$ belongs to.

\begin{figure}[h]
  \centering  
  \includegraphics[width=.3\textwidth]{figures/pdf/brunnian_4_doubled_crossings.pdf}
  \caption{Labeling the groups of four crossings in the intersection of
  $D^k(s_i)$ with $D^{k+1}(s_i)$}
  \label{fig:grouping_crossing}
\end{figure}

\begin{figure}[h]
  \begin{subfigure}[b]{0.45\textwidth}
    \centering  
    \includegraphics[width=.6\textwidth]{figures/pdf/label_crossing_a.pdf}
    \caption{Labeling when the vertical arcs pass over the horizontal
      ones: $z \in \{a,b,g,h\}$}
    \label{fig:labela}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \centering  
    \includegraphics[width=.6\textwidth]{figures/pdf/label_crossing_c.pdf}
    \caption{Labeling when the vertical arcs pass under the horizontal
      ones: $z \in \{c,d,e,f\}$}
    \label{fig:labelc}
  \end{subfigure}
  \caption{Labeling crossings within the groups of four}
  \label{fig:labeling}
\end{figure}
      

For a statement $s_i$ let $R(C^k)$ be the set of crossings $\{z_l(k)
\ | \ z \in \{a,b,c,d,e,f,g,h\}, 1 \le l \le 4\}$ that are at the
intersection between $C^k$ and $C^{k+1}$. Let $R(s_i)$ be the union of
$R(C^k)$ over every the $C^k$ that corresponds to a couple having $s_i$
as second argument. Let the weight of $s_i$ be the sum of the weights
of $R(s_i)$, that we will denote by $w(s_i)$.

\begin{lem}
  Let $s_i$ be a variable with $w(s_i) \le 1$. Let $r$ be the crossing in
  $R(s_i)$ which is the first of the crossings in $R(s_i)$ removed by an
  economical $II^-$ move (we allow a draw). Then one of the following
  cases holds
  \begin{itemize}
  \item $\{r, t(r)\} = \{b_2(C^0), b_4(C^0)\}$, $w(p_1(s_i)) = w(s_i) = 1$ and
    $p_1(s_i)$ is removed by a $I^-$ move prior to removing $r$ and
    $t(r)$.
  \item $\{r, t(r)\} \in \{\{b_2(C^k), b_4(C^k)\} \ | \ k \in [1,
    m_i]\}$, $w(s_i) = 0$.
  \end{itemize}
  \begin{proof}
    case analysis

    \begin{tabular}{|c|l|l|c|}
      \hline
      Choice of $r$ & $t(r)$ on $\alpha$ & $t(r)$ on $\beta$ &
      Overlap\\
      \hline
      $a_1$ & $\cancel{c_3}, \cancel{c_4}, a_2, b_1$ & $a_3, \cancel{c_1},
      d_4(k+1), d_3(k+1)$ & \\
      $a_2$ & $\cancel{c_3}, a_1, b_1, b_2$ & $a_4, \cancel{c_2},
      d_2(k+1), d_1(k+1)$ & \\
      $a_3$ & $\cancel{c_1}, \cancel{c_2}, a_4, b_3$ & $a_1,
      d_4(k+1), \cancel{c_1}, \cancel{c_3}$ &\\
      $a_4$ & $a_3, \cancel{c_1}, b_3, b_4$ & $a_2, d_2(k+1),
      \cancel{c_2}, \cancel{c_4}$& \\
      \hline
      $b_1, \dots b_4$ & separate case analysis & &\\
      \hline
      $c_1$ & $\cancel{a_1}, \cancel{a_3}, c_3, e_1$ & $\cancel{a_3}, \cancel{a_4}, c_2, d_1$ & \\
      $c_2$ & $\cancel{a_2}, \cancel{a_4}, c_4, e_2$ & $c_1, \cancel{a_3}, d_1, d_2$ &\\
      $c_3$ & $c_1,\cancel{a_3}, e_1, e_3$ & $\cancel{a_1}, \cancel{a_2}, c_4, d_3$ &\\
      $c_4$ & $c_2, \cancel{a_4}, e_2, e_4$ &  $c_3, \cancel{a_1}, d_3, d_4$ & \\
      \hline
      $d_1$ & $\cancel{b_1}, \cancel{b_3}, d_3, f_1$ & $c_1, c_2, d_2, a_2(k-1)$ &\\
      $d_2$ & $\cancel{b_2}, \cancel{b_4}, d_4, f_2$ & $d_1, c_2, a_2(k-1), a_4(k-1)$ &\\
      $d_3$ & $d_1, \cancel{b_3}, f_1, f_3$ & $c_3, c_4, d_4, a_1(k-1)$ & \\
      $d_4$ & $d_2, \cancel{b_4}, f_2, f_4$ & $c_4, d_3, a_1(k-1), a_3(k-1)$ & \\
      \hline
      $e_1$ & $c_1, c_3, e_3, \cancel{g_3}$ & $\cancel{g_3},
      \cancel{g_4}, e_2, f_1$ & \\
      $e_2$ & $c_2, c_4, e_4, \cancel{g_2}$ & $\cancel{g_3}, e_1, f_1,
      f_2$ & \\
      $e_3$ & $c_3, e_1, \cancel{g_1}, \cancel{g_3}$ & $\cancel{g_1},
      \cancel{g_2}, e_4, f_3$ & \\
      $e_4$ & $c_4, e_2, \cancel{g_2}, \cancel{g_4}$ & $\cancel{g_1},
      e_3, f_3, f_4$ & \\
      \hline
      $f_1$ & $d_1, d_3, f_3, \cancel{h_1}$ & $e_1, e_2, f_2,
      g_3(k-1)$ & \\
      $f_2$ & $d_2, d_4, f_4, \cancel{h_2}$ & $e_2, f_1, g_3(k-1),
      g_1(k-1)$ & \\
      $f_3$ & $d_3, f_1, \cancel{h_1}, \cancel{h_3}$ & $e_3, e_4, f_4,
      g_4(k-1)$ & \\
      $f_4$ & $d_4, f_2, \cancel{h_2}, \cancel{h_4}$ & $e_4, f_3,
      g_4(k-1), g_2(k-1)$ & \\
      \hline
      $g_1$ & $\cancel{e_3}, \cancel{e_4}, g_2, h_1$ & $\cancel{e_1},
      \cancel{e_3}, g_3, f_2(k+1)$ & \\
      $g_2$ & $\cancel{e_3}, g_1, h_2, h_1$ & $\cancel{e_2},
      \cancel{e_4}, g_4, f_4(k+1)$ & \\
      $g_3$ & $\cancel{e_2}, \cancel{e_1}, g_4, h_3$ & $\cancel{e_3},
      g_1, f_2(k+1), f_1(k+1)$ & \\
      $g_4$ & $\cancel{e_1}, g_3, h_3, h_4$ & $\cancel{e_4},
      g_2, f_4(k+1), f_3(k+1)$ & \\
      \hline
      $h_1$ & $g_1, g_2, h_2, \cancel{h_3(k-1)}$ & $\cancel{f_1}, \cancel{f_3},
      h_3, h_2(k+1)$ & \\
      $h_2$ & $g_2, h_1, \cancel{h_3(k-1)}, \cancel{h_1(k-1)}$ & $\cancel{f_2},
      \cancel{f_4}, h_4, h_4(k+1)$ & \\
      $h_3$ & $g_3, g_4, h_3, \cancel{h_4(k-1)}$ & $\cancel{f_3}, h_1,
      h_2(k+1), h_1(k+1)$ & \\
      $h_4$ & $g_4, h_3, \cancel{h_4(k-1)}, \cancel{h_2(k-1)}$ & $\cancel{f_4}, h_2,
      h_4(k+1), h_3(k+1)$ & \\
      \hline
    \end{tabular}

    \begin{tabular}{|c|c|l|l|c|}
      \hline
      Choice of $r$ & Case & $t(r)$ on $\alpha$ & $t(r)$ on $\beta$ &
      Overlap\\
      \hline
      $b_1$ &1 & $a_1, a_2, b_2, b_4$ & $b_3, d_1,$
      \colorbox{red}{$C^{k+2}$} & $\emptyset$ \\
      $b_2$ &1 &$a_2, b_1, b_4, b_3$ & $b_4, \cancel{d_2}, b_2(k-1),
      b_3(k-1)$ & $\{b_4\}$\\
      $b_3$ &1 &$a_3, a_4, b_4, b_2$ & $ b_1,$
      \colorbox{red}{$C^{k+2}$}, $\cancel{d_1}, \cancel{d_3}$&
      $\emptyset$\\
      $b_4$ &1 &$b_3, a_4, b_2, b_1$ & $b_2, b_1(k-1), d_2 d_4$ &
      $\{b_2\}$\\
      \hline
      $b_1$ &2 &$a_1, a_2, b_2, b_4$ & $b_1(k-1), b_3(k-1), b_3,
      \cancel{d_1}$ & $\emptyset$\\
      $b_2$ & &$a_2, b_1, b_4, b_3$ & $b_4, d_2,$
      \colorbox{red}{$C(k+2)$} & $\{b_4\}$\\
      $b_3$ &2 &$a_3, a_4, b_4, b_2$ & $b_1, b_1(k-1), \cancel{d_1},
      \cancel{d_3}$ & $\emptyset$\\
      $b_4$ &2 &$b_3, a_4, b_2, b_1$ & $b_2$, \colorbox{red}{$C(k+2)$},
      $\cancel{d_2}, \cancel{d_4}$ & $\{b_2\}$\\
      \hline
      $b_1$ &2 &$a_1, a_2, b_2, b_4$ & $b_3, \cancel{d_1}, $
      \colorbox{red}{$C(k+2)$} & $\emptyset$\\
      $b_2$ &3 &$a_2, b_1, p_1, b_4$ & $b_4, \cancel{d_2}, b_2(k+1), p_2$
       & $\{b_4\}$\\
      $b_3$ &3 &$a_3, a_4, b_4, p_1$ & $b_1,$ \colorbox{red}{$C(k+2)$}, $\cancel{d_1},
      \cancel{d_3}$ & $\emptyset$\\
      $b_4$ &3 &$b_3, a_4, b_2, p_1$ & $b_2, q 
      \cancel{d_2}, \cancel{d_4}$ & $\{b_2\}$\\
      \hline
      $p_1$ &3 & $b_3, b_4$ & $b_1, b_2$ & \\
      $p_2$ &3 &$b_1(k-1), b_3(k-1),$ \colorbox{red}{$C(k+2)$} &
      $b_2(k-1), b_4(k-1), b_2, b_4$ & \\
      \hline
 
    \end{tabular}
  \end{proof}
\end{lem}

\begin{lem}
\end{lem}
