\section{General case}

The aim of this section is to prove that by fixing the parameter of
the defect, the problem of untangling a diagram with shortest sequence
becomes $XP$. More precisely, we will prove that the problem:
\underline{\textsc{Input:}} a diagram $D$ of an unknot, and an integer
$n$,\\
\underline{\textsc{Parameter:}} an integer $d$ (that plays the role of
the defect of $D$),\\
\underline{\textsc{Question:}} does there exist a sequence $S$ of \Rms
of length less than $n$ that untangles $D$ ?\\

\begin{definition}
  Let $S$ be a sequence of \Rms. We define the set of \emph{special moves} of
  $S$ inductively:
  \begin{itemize}
  \item Every move $m$ of type other than $II^-$ is special.
  \item Every type $II^-$ \Rm $\{a, b\}_{II^-}$ such that $a$ or $b$
    is involved in a special move is a special move. More precisely,
    if $a$ (or $b$) verifies one of the following conditions:
    \begin{itemize}
    \item $a$ (or $b$) is a crossing added by a $I^+$ or $II^+$ move of $S$,
    \item $a$ (or $b$) is the endpoint of an arc on which a $I^+$ or $II^+$ move
      is performed,
    \item $a$ (or $b$) is involved in a type $III$ \Rm,
    \end{itemize}
    then $\{a, b\}_{II^-}$ is a special move.
  \end{itemize}
  We call the subsequence of special moves of a sequence $S$ the
  \emph{special subsequence of $S$} and the crossings involved in the
  special moves the \emph{special crossings}.

  The non-special crossings are thus affected by at most one move (of
  type $II^-$).
\end{definition}

\begin{definition}
  \label{similar}
  Let $S$ and $S'$ be two sequences of \Rms of respective subsequences
  $T = (m_1, \dots m_p)$ and $T' = (m_1', \dots m_q')$.  We say that
  $S$ and $S'$ are similar if they remove the same set of crossing and
  $p = q$ and
  \begin{align*}
    \forall i, m_i =
    \left\{
      \begin{array}{ll}
        m_i' & \text{if } m_i' \text{ is of type other than } II^+\\
        \{(a)^{\epsilon_a}, (cd), side, \pm turn\}_{II^+} & \text{if } m_i' = \{(a)^{\epsilon_a},
        (cd), side, turn\}_{II^+}
      \end{array}
      \right.
  \end{align*}
  This similarity between sequences is an equivalence relation.
\end{definition}

\begin{lem}
  \label{commute}
  Let $D$ be a diagram in which the sequences $S_1 = (\{c_i,
  c_j\}_{II^-}, m)$ and $S_2 = (m, \{c_i, c_j\}_{II^-}$ are both
  feasible, with $m$ an arbitray of type other than $II^+$ move that
  does not involve $c_i$ or $c_j$. Then, $S_1$ and $S_2$ lead to the
  same diagram.
  \begin{proof}
    Let us assume that $m$ is not of type $II^+$.  Both moves are
    feasible in $D$, there is 1-Sphere that contains $c_i$ and $c_j$
    and no other crossings of the diagram, and an other that contains
    the crossings involved in $m$ and no other crossings. Each move
    results in a local change and does not change the rest of the
    diagram as depicted in Figures \ref{fig:RM1}, \ref{fig:RM2} and
    \ref{fig:RM3}.
  \end{proof}
\end{lem}

\begin{thm}
  \label{untangling}
  Let $D$ be a diagram of an unknot that can be untangled with a
  sequence $S = (\{c_1, c_2\}_{II^-}, \dots \{c_{2k-1},
  c_{2k}\}_{II^-}, m_1, \{c_{2k+1}, c_{2k+2}\}_{II^-} \dots m_2
  \dots)$, where $c_1, \dots c_{2N}$ are the non-special crossings and
  $m_1, \dots m_n$ are the special moves.

  Let $c_i$ and $c_j$ be two non special crossings such that $\{c_i,
  c_j\}_{II^-}$ is feasible in $D$. It is possible to untangle $D$
  with a sequence of \Rms that starts with $\{c_i, c_j\}_{II^-}$ and
  that is similar to $S$ (see Definition \ref{similar})

  \begin{proof}
    We can assume without loss of generality that $i<j$, and that $i$
    is odd while $j$ is even (by relabeling the two crossings involved
    in the type $II^-$ \Rm removing $c_i$, respectively $c_j$, which
    does not change the move). There are two possible cases:
    \begin{itemize}
    \item $\{c_i, c_j\}_{II^-}$ is a move of $S$. We have $c_j =
      c_{i+1}$.

      \noindent\fbox{\parbox{\linewidth-2\fboxrule-2\fboxsep}{$S$ has
          the form $(\underbrace{\{c_1, c_2\}_{II^-}, \dots}_{S_1}
          \{c_{i}, c_{i+1}\}_{II^-}, \underbrace{\dots}_{S_2})$. One
          may notice that the subsequences $S_1$ and $S_2$ may contain
          special moves. We will show that the sequence $(\{c_i,
          c_{i+1}\}_{II^-}, \tilde{S_1}, S_2)$ is feasible in $D$,
          with $\tilde{S_1}$ similar to $S_1$ (see Definition
          \ref{similar}). To do that, we recursively switch the \Rm
          $\{c_i, c_{i+1}\}_{II^-}$ with its predecessor $p$ in $S$,
          to pull $\{c_i,c_{i+1}\}_{II^-}$ back at the beginning of
          $S$.}}

      The crossings $c_i$ and $c_{i+1}$ are not affected by $p$,
      because $\{c_i, c_j\}_{II^-}$ is not a special \Rm. Let $D_1$ be
      the diagram obtained from $D$ in the course of $S$, just before
      $p$ is performed.

      The \Rm $\{c_i, c_{i+1}\}_{II^-}$ is feasible in $D$, thus $c_i$
      and $c_{i+1}$ are 0-close in $D$ with the respect to the set $C$
      of all the other crossings of $D$. Let $\alpha$ and $\beta$ be
      the arcs connecting $c_i$ and $c_j$ from Definition
      \ref{2closeness}. Lemma \ref{resistance2-} shows that $c_i$ and
      $c_j$ are still 0-close in $D_1$ with respect to $C$. In order to
      know if $\{c_i, c_{i+1}\}_{II^-}$ is feasible in $D_1$, we need
      to show that $c_i$ and $c_{i+1}$ are 0-close not only in respect
      to the crossings of $C$ that were not deleted, but also if we
      consider the crossings introduced by type $I^+$ and $II^+$ moves
      as well. Let $\alpha'$ and $\beta'$ be the arcs descended from
      $\alpha$ and $\beta$. If a crossing was introduced in $\alpha'$
      or $\beta'$ this would mean that there was a $I^+$ or $II^+$ move
      using $\alpha$ or $\beta$ which is impossible since $c_i$ and
      $c_{i+1}$ are not special crossings. In the end, $\{c_i,
      c_{i+1}\}_{II^-}$ is feasible in $D_1$.

      We will now show that $(\{c_i, c_{i+1}\}_{II^-}, p)$ is feasible
      in $D_1$. Let us suppose for now that $p$ is of type other than
      $II^+$.  We already know that $\{c_i, c_{i+1}\}_{II^-}$ is
      feasible, let $D_2$ be the diagram obtained after it is
      performed. Several cases can occur:

      \begin{itemize}
      \item $p$ is of type $II^-$: $p = \{a, b\}_{II^-}$.\\ Since
        $\{a, b\}_{II^-}$ was feasible in $D_1$, $a$ and $b$ are
        0-close in $D_1$ with respect to the set of remaining crossings
        $C$ (Lemma \ref{feasible2-}). Then Lemma \ref{2closeresist}
        shows that $a$ and $b$ are 0-close in $D_2$ with respect to
        $C\setminus \{c_i, c_{i+1}\}$ which is exactly the set of
        crossings of $D_2$. Thus we can apply Lemma \ref{resistance2-}
        to prove that $p$ is feasible in $D_2$.
        
      \item $p$ is of type $I^-$: $p = \{a\}_{I^-}$.\\
        We can use the same reasonning as in the first case with
        Lemmas \ref{feasible1-}, \ref{1closeresist} and
        \ref{resistance1-} to prove that $p$ is feasible in $D_2$.
        %% We can start by
        %% performing $\{c_i, c_{i+1}\}_{II^-}$, let $D_2$ be the diagram
        %% obtained. Since $\{a\}_{I^-}$ was feasible in $D_1$, $a$ is
        %% 0-self-close in $D_1$ with respect to the set of remaining
        %% crossings $C$ (Lemma \ref{feasible1-}). Then Lemma
        %% \ref{resistance1-} shows that $a$ is 0-self-close in $D_2$
        %% with respect to $C\setminus \{c_i, c_{i+1}\}$ which is exactly the
        %% set of remaining crossings of $D_2$. Likewise, the second
        %% condition of Lemma \ref{feasible1-} is verified in $D_1$ and in
        %% $D_2$ (Lemma \ref{resistance1-}), thus $\{a\}_{I^-}$ remains
        %% feasible in $D_2$. To sum up, both sequences $(\{a\}_{I^-},
        %% \{c_i, c_{i+1}\}_{II^-})$ and $(\{c_i, c_{i+1}\}_{II^-},
        %% \{a\}_{I^-})$ can be performed in $D_1$.

        %% In both sequences the move $\{c_i, c_{i+1}\}_{II^-}$ affects
        %% the diagram identically and locally, only in the neighborhood
        %% of the crossings $c_i$ and $c_{i+1}$ (Proposition \ref{samediag}). In
        %% both sequences $\{a\}_{I-}$ affect only the neighborhood $a$
        %% by removing the loop around $a$ and this loops does not
        %% intersect any other crossings, thus $\{a\}_{I^-}$ has also a
        %% local effect and both sequences lead to the same diagram.
        
      \item $p$ is of type $III$: $p = \{a,b,c\}_{III}$.\\
        We can use the same reasonning as in the first case with
        Lemmas \ref{feasible3}, \ref{3closeresist} and
        \ref{resistance3} to prove that $p$ is feasible in $D_2$.

        %% We can start by performing $\{c_i, c_{i+1}\}_{II^-}$, let
        %% $D_2$ be the diagram obtained. Since $\{a, b,c\}_{III}$ was
        %% feasible in $D_1$, $(a, b, c)$ is 0-close in $D_1$ with respect
        %% to the set of remaining crossings $C$ (Lemma
        %% \ref{feasible3}). Then Lemma \ref{resistance3} shows that
        %% $(a, b, c)$ is 0-close in $D_2$ with respect to the
        %% $C\setminus \{c_i, c_{i+1}\}$ which is exactly the set of
        %% remaining crossings of $D_1$. Likewise, the second condition of
        %% Lemma \ref{feasible3} is verified in $D_1$ and in $D_2$ (Lemma
        %% \ref{resistance3}), thus $\{a, b, c\}_{III}$ remains feasible
        %% in $D_2$. To sum up, both sequences $(\{a, b, c\}_{III},
        %% \{c_i, c_{i+1}\}_{II^-})$ and $(\{c_i, c_{i+1}\}_{II^-}, \{a,
        %% b, c\}_{III})$ can be performed in $D_1$.

        %% In both sequences, the move $\{c_i, c_{i+1}\}_{II^-}$ affects
        %% the diagrams identically and locally, only in the neighborhood
        %% of the crossings $c_i$ and $c_{i+1}$ (Proposition \ref{samediag}). In
        %% both sequences, no crossing lies in the triangle $(a, b, c)$
        %% before performing the move $\{a, b, c\}_{III}$ and neither
        %% $c_i$ nor $c_{i+1}$ is equal to $a$, $b$ or $c$ since $a$, $b$
        %% and $c$ are special crossings whereas $c_i$ and $c_{i+1}$ are
        %% not. Thus, in both sequences, $\{a, b, c\}_{III}$ has only a
        %% local effect on the triangle $(a, b, c)$ and not on the rest
        %% of the diagram. In the end, both sequences lead to the same
        %% diagram.
        
      \item $p$ is of type $I^+$: $p = \{(a)^{\epsilon_a}, w, side\}_{I^+}$. \\
        Lemma \ref{feasible1+} shows that in $D_2$,
        $\{(a)^{\epsilon_a}, w, side\}_{I^+}$ remains feasible and can
        be applied as well.

      \end{itemize}

      We just proved that $(\{c_i, c_{i+1}\}_{II^-}, p)$ is feasible
      in $D_1$. Lemma \ref{commute} shows that the sequences $(\{c_i,
      c_{i+1}\}_{II^-}, p)$ and $(p,\{c_i, c_{i+1}\}_{II^-})$ lead to
      the same diagram. Thus $p$ and $\{c_i, c_{i+1}\}_{II^-}$ can be
      switched in $S$.

      Remains the case where $p$ is of type $II^+$: $p =
      \{(a)^{\epsilon_a},(b)^{\epsilon_b}, side, turn\}_{II^+}$. Since
      $c_i$ and $c_{i+1}$ are non-special crossings, they do not
      belong to $\{a, b\}$ and Lemma \ref{resistance2+} shows that in
      the new diagram obtained, $\{(a)^{\epsilon_a}, (b)^{\epsilon_b},
      side, turn\}_{I^+}$ and $\{(a)^{\epsilon_a}, (b)^{\epsilon_b},
      side, turn\}_{I^+}$ are feasible. Lemma \ref{resistance2+} also
      shows that the diagram obtained from $D_1$ by applying $(p,
      \{c_i, c_{i+1}\}_{II^-})$ can also be obtained from $D_1$ by
      applying $(\{c_i, c_{i+1}\}_{II^-}, \{(a)^{\epsilon_a},
      (b)^{\epsilon_b}, side, turn\}_{II^+})$ or $(\{c_i,
      c_{i+1}\}_{II^-}, \{(a)^{\epsilon_a}, (b)^{\epsilon_b}, side,
      -turn\}_{II^+})$.

      Thus $\{c_i, c_{i+1}\}_{II^-}$ can be switched with $p$, with
      a possible change of the sign of $turn$ at the same time.
      
    \item $c_i$ and $c_j$ are removed by two different moves in
      $S$. Since we assumed that $i$ is odd (respectively that $j$ is
      even), $c_i$ (respectively $c_j$) is removed in $S$
      by the move $\{c_i, c_{i+1}\}_{II^-}$ (respectively by
      $\{c_{j-1}, c_j\}_{II^-}$).

      \noindent\fbox{\parbox{\linewidth-2\fboxrule-2\fboxsep}{$S$ has
          the form $(\underbrace{\{c_1, c_2\}_{II^-}, \dots}_{S_1}
          \{c_{i}, c_{i+1}\}_{II^-}, \underbrace{\dots}_{S_2}
          \{c_{j-1},c_{j}\}_{II^-}, \underbrace{\dots}_{S_3})$. One
          may notice that the subsequences $S_1$, $S_2$ and $S_3$ may
          contain special moves. We will prove that the sequence
          $(\{c_i, c_j\}_{II^-}, \tilde{S_1}, \tilde{S_2}, \{c_{i+1},
          c_{j-1}\}_{II^-}, S_3)$ is also feasible and untangles $D$,
          with $\tilde{S_1}$ similar to $S_1$ and $\tilde{S_2}$
          similar to $S_2$ (see Definition \ref{similar}). In order to
          do that, we will first show that the sequence $(S_1,
          \hat{S_2}, \{c_i, c_{i+1}\}_{II^-}, \{c_{j-1},
          c_{j}\}_{II^-}, S_3)$ is feasible as well and also untangles
          the knot, with $\hat{S_2}$ similar to $S_2$, by recursively
          switching $\{c_i, c_{i+1}\}_{II^-}$ with its successor
          $s$. We will then show that the sequence $(S_1, \hat{S_2},
          \{c_i, c_{j}\}_{II^-}, \{c_{i+1}, c_{j-1}\}_{II^-}, S_3)$ is
          also feasible and untangles the knot. Then the first case of
          this proof can be applied to conclude.}}

      \textbf{First step :}
      
      We first want to prove that the sequence $(S_1, S_2, \{c_i,
      c_{i+1}\}_{II^-}, \{c_{j-1}, c_{j}\}_{II^-}, S_3)$ is also
      feasible and untangles the knot, by recursively switching
      $\{c_i, c_{i+1}\}_{II^-}$ with its successor $s$.

      Let $D_1$ be the diagram obtained from $D$ in the course of $S$,
      just before $\{c_i, c_{i+1}\}_{II^-}$ is performed. The same
      reasonning as in the previous case shows that $\{c_i,
      c_j\}_{II^-}$ is feasible in $D_1$. Let $D_2$ be the diagram
      obtained from $D_1$ after $\{c_i, c_{i+1}\}_{II^-}$.

      Let us assume for now that $s$ is of type other than $II^+$.
      
      \begin{itemize}
      \item $s$ is of type $II^-$: $s = \{a, b\}_{II^-}$.\\ $s$ is
        feasible in $D_2$, thus, $a$ and $b$ are 0-close with respect
        to the set $C$ of all the other crossings of $D_2$. Thanks to
        Lemma \ref{resistance2-}, $a$ and $b$ are also 0-close in $D_1$
        with respect to $C$. 

        To prove that $s$ is feasible in $D_1$, we only need to prove
        that $a$ and $b$ are 0-close in regard to $C \cup \{c_i,
        c_{i+1}\}$ in $D_1$. Let $\alpha$ and $\beta$ be the arcs used
        for the 0-closeness of $a$ and $b$ in $D_1$. Since $c_j \in C$,
        $\alpha$ and $\beta$ do not contain $c_j$. Moreover, $\{c_i,
        c_j\}_{II^-}$ is feasible in $D_1$ so the crossings $c_j$ and
        $c_i$ are just next to one another and $c_i$ do not belong to
        the interior or $\alpha$ or $\beta$ either. Likewise, $\{c_i,
        c_{i+1}\}_{II^-}$ is feasible in $D_1$ so $c_{i+1}$ does not
        belong to the interior of $\alpha$ or $\beta$.

        This proves that $a$ and $b$ are 0-close in $D_1$ with respect
        to all the other crossings of $D_1$, thus Lemma
        \ref{resistance2-} can be applied and $\{a, b\}_{II^-}$ is
        feasible in $D_1$. Let $D_3$ be the diagram obtained, $c_i$
        and $c_{i+1}$ are 0-close $D_3$ (Lemma \ref{2closeresist}) so
        $\{c_i, c_{i+1}\}_{II^-}$ remains feasible in $D_3$ thanks to
        Lemma \ref{resistance2-}. To sum up, both sequences $(\{a,
        b\}_{II^-}, \{c_i, c_{i+1}\}_{II^-})$ and $(\{c_i,
        c_{i+1}\}_{II^-}, \{a, b\}_{II^-})$ can be performed in $D_1$.

        
      \item $s$ is of type $I^-$: $s = \{a\}_{I^-}$.\\ We can use the
        same reasonning as in the first case with Lemmas
        \ref{resistance1-}, \ref{1closeresist} and \ref{resistance2-}
        to prove that $(\{a\}_{I^-}, \{c_i, c_{i+1}\}_{II^-})$ is
        feasible in $D_1$.
        
        %% Let $D_2$ be
        %% the diagram obtained from $D_1$ after $\{c_i,
        %% c_{i+1}\}_{II^-}$. $s$ is feasible in $D_2$, thus, $a$ is
        %% 0-self-close with respect to the set of remaining crossings
        %% $C$ and verify the second condition of Lemma
        %% \ref{feasible1-}. Thanks to Lemma \ref{resistance1-}, this is
        %% also the case in $D_1$. To prove that $s$ is feasible in $D_1$,
        %% we only need to prove that $a$ is 0-self-close with respect to
        %% $C \cup \{c_i, c_{i+1}\}$ in $D_1$. The arc $\alpha$ used for
        %% the 0-self-closeness of $a$ in $D_1$ do not contain $c_j$ and
        %% since $\{c_i, c_j\}_{II^-}$ is feasible in $D_1$, $c_j$ and
        %% $c_i$ are just next to one another and $c_i$ does not belong
        %% to $\alpha$ either. We prove similarly that $c_{i+1}$ does not
        %% belong to $\alpha$ by considering the feasible move $\{c_i,
        %% c_{i+1}\}_{II^-}$ in $D_1$.

        %% We just proved that we can start by performing $s$, let $D_3$
        %% be the diagram obtained. $\{c_i, c_{i+1}\}_{II^-}$ remains
        %% feasible in $D_3$ thanks to Lemma \ref{resistance2-}. To sum
        %% up, both sequences $(\{a\}_{I^-}, \{c_i, c_{i+1}\}_{II^-})$
        %% and $(\{c_i, c_{i+1}\}_{II^-}, \{a\}_{I^-})$ can be performed
        %% in $D_1$.

        %% The same reasonning as the one used in the firts case can be
        %% applied to show that the obtained diagrams are identical.
        
      \item $s$ is of type $III$: $s = \{a, b, c\}_{III}$.\\
        We can use the same reasonning as in the first case with
        Lemmas \ref{resistance1-}, \ref{1closeresist} and
        \ref{resistance2-} to prove that $(\{a, b, c\}_{III}, \{c_i,
        c_{i+1}\}_{II^-})$ is feasible in $D_1$.

        %% Let $D_2$ be the diagram obtained from $D_1$ after $\{c_i,
        %% c_{i+1}\}_{II^-}$. $s$ is feasible in $D_2$, thus, $(a, b, c)$
        %% is 0-close with respect to the set of remaining crossings $C$
        %% of $D_2$ and verify the second condition of Lemma
        %% \ref{feasible3}. Thanks to Lemma \ref{resistance3}, this is
        %% also the case in $D_1$. To prove that $s$ is feasible in $D_1$,
        %% we only need to prove that $(a, b, c)$ is 0-close with respect
        %% to $C \cup \{c_i, c_{i+1}\}$ in $D_1$. The arcs $\alpha$,
        %% $\beta$ and $\gamma$ used for the 0-self-closeness of $(a, b,
        %% c)$ in $D_1$ do not contain $c_j$ and since $\{c_i,
        %% c_j\}_{II^-}$ is feasible in $D_1$, $c_j$ and $c_i$ are just
        %% next to one another and $c_i$ does not belong to $\alpha$,
        %% $\beta$ or $\gamma$ either. We prove similarly that $c_{i+1}$
        %% does not belong to $\alpha$, $\beta$ or $\gamma$ by
        %% considering the feasible move $\{c_i, c_{i+1}\}_{II^-}$ in
        %% $D_1$.

        %% We just proved that we can start by performing $s$, let $D_3$
        %% be the diagram obtained. $\{c_i, c_{i+1}\}_{II^-}$ remains
        %% feasible in $D_3$ thanks to Lemma \ref{resistance2-}. To sum
        %% up, both sequences $(\{a, b, c\}_{III}, \{c_i, c_{i+1}\}_{II^-})$
        %% and $(\{c_i, c_{i+1}\}_{II^-}, \{a, b, c\}_{III})$ can be performed
        %% in $D_1$. Furthermore, they lead to the same diagram by
        %% applying the same reasonning as in the first case.
        
      \item $s$ is of type $I^+$: $s = \{(a)^{\epsilon_a}, w, turn\}_{I^+}$.\\
        We can start by performing $s$ since type $I^+$ \Rms are
        always feasible (Lemma \ref{feasible1+}), let $D_2$ be the
        diagram obtained. The \Rm $\{c_i, c_{i+1}\}_{II^-}$ was feasible in
        $D_1$, so $c_i$ and $c_{i+1}$ are 0-close in $D_1$ with respect
        to the set of remaining crossings $C$. Lemma
        \ref{resistance2-}, shows that it is also the case in $D_2$,
        with respect to $C$. To prove that $\{c_i, c_{i+1}\}_{II^-}$
        is feasible in $D_2$, we need to show that they are 0-close in
        regard to $C \cup {b}$ where $c$ is the new crossing
        introduced by $s$. Now, $a$ is not on the arcs
        $\alpha$ and $\beta$ used for the 0-closeness of $c_i$ and
        $c_{i+1}$, so $b$ is not in the interior of $\alpha$ and
        $\beta$ either.

        To sum up, both sequences $(\{(a)^{\epsilon_a}, w, turn\}_{II^-}, \{c_i,
        c_{i+1}\}_{II^-})$ and $(\{c_i, c_{i+1}\}_{II^-}, \{(a)^{\epsilon_a},
        w, turn\}_{II^-})$ can be performed in $D_1$.         

      \end{itemize}

      We just proved that $(s, \{c_i, c_{i+1}\}_{II^-})$ is feasible
      in $D_1$. Lemma \ref{commute} shows that the sequences $(\{c_i,
      c_{i+1}\}_{II^-}, s)$ and $(s,\{c_i, c_{i+1}\}_{II^-})$ lead to
      the same diagram. Thus $s$ and $\{c_i, c_{i+1}\}_{II^-}$ can be
      switched in $S_2$.
      
      Remains the case where $s$ is of type $II^+$: $s =
      \{(a)^{\epsilon_a},(b)^{\epsilon_b}, side, turn\}_{II^+}$. We
      first need to prove that $s$ is feasible in $D_1$.  As a
      reminder, $D_2$ is obtained from $D_1$ by performing $\{c_i,
      c_{i+1}\}_{II^-}$. As we observed before, type $II^-$ \Rms are
      local, there exists a disk that contains $c_i$, $c_j$, the arcs
      they lie on and no other crossings or arcs of the diagram (as
      depicted on Figure \ref{fig:RM2_faces}) such that the only
      difference between $D_1$ and $D_2$ is contained in the
      disk. Thus, all the faces of $D_1$ but the ones that are
      incident to $c_i$ and $c_j$ are preserved in $D_2$. Let $\alpha$
      and $\beta$ be the two arcs connecting $c_i$ and $c_{i+1}$ in
      $D_1$ from Definition \ref{2closeness}. Let $F_{\alpha}$,
      $F_{\beta}$, $F_{i}$ and $F_{i+1}$ be the faces labeled on
      Figure \ref{fig:RM2_faces}.

      \begin{figure}[h]
        \centering  
        \includegraphics[width=.7\textwidth]{../figures/pdf/RMII_faces.pdf}
        \caption{The move $\{c_i, c_{i+1}\}_{II^-}$ is local}
        \label{fig:RM2_faces}
      \end{figure}

      In $D_2$, let $F_{\alpha}'$ and $F_{\beta}'$ be the faces
      descended from $F_{\alpha}$ and $F_{\beta}$ respectively. Let
      $F$ be the face of $D_2$ obtained when merging $F_i$ with
      $F_{i+1}$.

      Since the move $\{c_i, c_{i+1}\}_{II^+}$ is local, for all
      crossing $c$ other than $c_i$ and $c_j$, that is incident to
      $F_{\alpha}$ (respectively $F_{\beta}$), the arc
      $(c)^{\epsilon_c}$ is incident to $F_{\alpha}$ (respectively
      $F_{\beta}$) in $D_1$ if and only if the arc $(c)^{\epsilon_c}$
      is incident to $F_{\alpha}'$ (respectively $F_{\beta}'$) in
      $D_2$. Likewise, for all crossing $c$ other than $c_i$ and
      $c_j$, that is incident to $F_i$ or $F_{i+1}$ in $D_1$, the arc
      $(c)^{\epsilon_c}$ is incident to $F_i$ or $F_{i+1}$ in $D_1$ if and
      only if $(c)^{\epsilon_c}$ is incident to $F$ in $D_2$.
      
      We recall that $s$ is feasible in $D_2$. Let $F_1$ and $F_2$ be
      the faces of $D_1$ and $D_2$ that are incident to
      $(a)^{\epsilon_a}$ and encoded by $side$. Since $s$ is feasible,
      $(b)^{\epsilon_b}$ is incident to $F_2$ in $D_2$, so if $F_2
      \neq F$, the preceding arguments shows that $(b)^{\epsilon_b}$
      is incident to $F_1$ in $D_1$. 

      As a reminder, $\{c_i, c_j\}_{II^-}$ is feasible in $D_1$, thus
      the crossings $c_i$, $c_{i+1}$ and $c_j$ are arranged as shown
      on Figure \ref{fig:config1} and $F_i$ is incident only to the
      crossings $c_j$ and $c_i$. If, $F_2 = F$ then $(a)^{\epsilon_a}$
      and $(b)^{\epsilon_b}$ are incident to $F_{i+1}$ in $D_1$
      because $a$ and $b$ are special crossings whereas $c_i$ and
      $c_j$ are not. 
      
      Thanks to Lemma \ref{feasible2+}, this prove that we can
      performing $s$ in $D_1$, let $D_3$ be the diagram obtained. The
      \Rm $\{c_i, c_{i+1}\}_{II^-}$ was feasible in $D_1$, so $c_i$
      and $c_{i+1}$ are 0-close in $D_1$ with respect to the set of
      remaining crossings $C$. Lemma \ref{2closeresist}, shows that it
      is also the case in $D_3$, with respect to $C$. To prove that
      $\{c_i, c_{i+1}\}_{II^-}$ is feasible in $D_3$, we need to show
      that $c_i$ and $c_{i+1}$ are 0-close with respect to $C \cup
      \{c, d\}$ where $c$ and $d$ are the new crossings introduced by
      $s$. This is the case because $a$ and $b$ are not on the bigon
      between $c_i$ and $c_{i+1}$ in $D_1$, so $c$ and $d$ are not on
      the bigon between $c_i$ and $c_{i+1}$ in $D_3$ either. Lemma
      \ref{resistance2-} can be applied and $\{c_i, c_{i+1}\}_{II^-}$
      is feasible in $D_3$.

      To sum up, both sequences $(\{(a)^{\epsilon_a},
      (b)^{\epsilon_b}, side, turn\}_{II^+}, \{c_i, c_{i+1}\}_{II^-})$
      and $(\{c_i, c_{i+1}\}_{II^-}, \{(a)^{\epsilon_a},
      (b)^{\epsilon_b}, side, turn\}_{II^+})$ can be performed in
      $D_1$. Lemma \ref{resistance2+} shows that the diagram obtained
      from $D_1$ after $(\{(a)^{\epsilon_a}, (b)^{\epsilon_b}, side,
      turn\}_{II^+}, \{c_i, c_{i+1}\}_{II^-})$ can be obtained from
      $D_1$ after $(\{c_i, c_{i+1}\}_{II^-}, \{(a)^{\epsilon_a},
      (b)^{\epsilon_b}, side, \pm turn\}_{II^+})$, which proves that
      the two moves can be switched, with a possible change of sign of
      $turn$ at the same time.


      We just proved that there exists a sequence $\hat{S_2}$ similar
      to $S_2$ such that $(S_1, \hat{S_2}, \{c_i, c_{i+1}\}_{II^-},
      \{c_{j-1}, c_j\}_{II^-}, S_3)$ is feasible in $D$ and untangles
      the knot.

      \textbf{Second step :}
      
      We now want to prove that the sequence $(S_1, \hat{S_2},
      \{c_i, c_{j}\}_{II^-}, \{c_{i+1}, c_{j-1}\}_{II^-}, S_3)$ is
      also feasible and untangles the knot. Let $D_1$ be the diagram
      obtained from $D$ after the sequence $(S_1, S_2)$. In $D_1$,
      $\{c_i, c_{i+1}\}_{II^-}$ is feasible and $\{c_i, c_j\}_{II^-}$
      is feasible as well (this can be proven with the same reasonning
      as in the first case once again).

      
      Let us denote by $D_2$ the diagram obtained from $D_1$ after the
      move $\{c_i, c_{i+1}\}_{II^-}$. The crossings $c_{j-1}$ and
      $c_{j}$ are 0-close in $D_2$ with respect to the set of
      remaining crossings $C$ (Lemma \ref{feasible2-}), thus $c_{j-1}$
      and $c_j$ are also 0-close in respect to $C$ in $D_1$ (Lemma
      \ref{2closeresist}). One may notice that $c_i, c_{i+1} \notin
      C$.
      
      The only piece of diagram in which $\{c_i, c_j\}_{II^-}$ is
      feasible and $\{c_i, c_{i+1}\}_{II^-}$ is feasible as well is
      shown in figure \ref{fig:config1}.

      \begin{figure}[h]
        \centering  
        \includegraphics[width=.5\textwidth]{../figures/pdf/config1.pdf}
        \caption{Only configuration in which $\{c_i, c_j\}_{II^-}$
          and $\{c_i, c_{i+1}\}_{II^-}$ are feasible}
        \label{fig:config1}
      \end{figure}
      
      If we add $c_{j-1}$ in this representation, there are two
      possible cases:
      \begin{itemize}
      \item $c_j$ and $c_{j-1}$ are 0-close with respect to all the
        other crossings, including $c_i$ and $c_{i+1}$;
      \item $c_j$ and $c_{j-1}$ are 0-close with respect to all the
        other crossings, except $c_i$ and $c_{i+1}$, and $c_i$,
        $c_{i+1}$ are in the interior of $\alpha$ or $\beta$, where
        $\alpha$ and $\beta$ are the arcs connecting $c_j$ and
        $c_{j-1}$ from Definition \ref{2closeness}.
      \end{itemize}


      In the first case, Lemma \ref{resistance2-} implies that
      $\{c_{j-1}, c_j\}_{II^-}$ is feasible as well in $D_1$. So the
      only possible configuration for the first case is represented in
      figure \ref{fig:config2}. In the second case, the configuration
      of Figure \ref{fig:config3}, its vertical symmetry and the
      configuration of Figure \todo{figure}
      are possible depending on whether
      only one of the arcs $\alpha$ and $\beta$ contains $c_i$ and
      $c_{i+1}$ or both.

      \begin{figure}[h]
        \begin{subfigure}[b]{0.55\textwidth}
          \centering  
          \includegraphics[width=\textwidth]{../figures/pdf/config2.pdf}
          \caption{Only possible configuration if additionally $\{c_j,
            c_{j-1}\}_{II^-}$ is feasible}
          \label{fig:config2}
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{0.4\textwidth}
          \centering  
          \includegraphics[width=\textwidth]{../figures/pdf/config3.pdf}
          \caption{Only possible configuration if additionnaly $c_j$
            and $c_{j-1}$ are 2-close but not 0-close}
          \label{fig:config3}
        \end{subfigure}
        \caption{Only two possible configurations for the crossings
          $c_i$, $c_{i+1}$, $c_{j-1}$ and $c_j$}
        \label{fig:config}
      \end{figure}
      
      In the all configurations, the sequence $(\{c_i, c_j\}_{II^-},
      \{c_{i+1}, c_{j-1}\}_{II^-})$ is feasible and this sequence
      leads as the same diagram as $(\{c_i, c_{i+1}\}_{II^-},
      \{c_{j-1}, c_j\}_{II^-})$ (Lemma \ref{commute}).

      We just proved that the sequence $(S_1, \hat{S_2}, \{c_i, c_j\}_{II^-},
      \{c_{i+1}, c_{j-1}\}_{II^-}, S_3)$ is feasible.

      \textbf{Third Step :}
      We can now conclude by applying the first case of the proof, to
      pull $\{c_i, c_j\}_{II^-}$ at the beginning of the sequence.

    \end{itemize}

    In both cases, we proved that there exists a sequence similar to
    $S$ that starts by $\{c_i, c_j\}_{II^-}$ and untangles the diagram.
  \end{proof}
\end{thm}

\section{A greedy algorithm to untangle the diagram}


Suppose that a diagram of an unknot $D$ can be untangled by a sequence
$S$ of defect $d$ and that we are given the special subsequence $S'$
of $S$. The algorithm \ref{algo} untangles $D$.

\begin{algorithm}[H]
  \SetKwInOut{Input}{input}\SetKwInOut{Output}{output}

  \Input{A diagram $D$ of an unknot and its defect $d$}
  \BlankLine
  Compute $E$ the set of all special subsequences $S'$ of defect $d$\;
  \For{every special subsequence $S' \in E$}{
    \While{ $D$ is not untangled}{
      \While{a type $II^-$ \Rm $m$ is feasible}{
        perform $m$\;}
      \eIf{the first move $m$ of $S'$ is feasible}{
        perform $m$\;
        $S' \leftarrow tail(S')$\;
      }{
        break\;
      }
    }
  }
  \label{algo}
  \caption{Greedy algorithm untangling a diagram of known defect}
\end{algorithm}   

Indeed, Theorem \ref{untangling} states that for every type $II^-$
feasible move $m$, there exists a sequence $\hat{S}$ starting by $m$
and similar to $S$ that untangles the diagram, thus if we execute in
parallel the first \emph{while} loop on all possible special
subsequences $S'$, after each move is performed, $E$ contains the special
sequence of an untangling sequence.

Let $n$ be the size of $D$ and $S'$ be a special subsequence. At most
$d/2$ crossings are inserted by $S'$ in type $I^+$ or $II^+$ moves,
thus at any moment there are at most $N = n + d/2$ crossings in the
diagram. Moreover, $S'$ is composed of at most $2d$ moves because each
modified or inserted crossing must be removed by $S'$. For each of
these moves, let use enumerate the possiblities:
\begin{itemize}
\item for a type $II^-$ move, one must choose two crossings among at most
  $N$ crossings,
\item for a type $I^-$ move, one need only to choose the crossing on which
  it is performed among at most $N$ crossings, 
\item for a type $III$ move, one need only to choose the three
  crossings on which it is performed among at most $N$ crossings,
\item for a type $I^+$ move, one need to choose one crossing among at
  most $N$ crossings and to set the binary variables $\epsilon$, $w$
  and $side$ which makes $8N$ possibilities, 
\item for a type $I^+$ move, one need to choose two crossings among at
  most $N$ crossings and to set the binary variables $\epsilon_a$, $\epsilon_b$,
  $side$ and $turn$ which makes in total $16 N(N-1)$ possibilities,
\end{itemize}

In the end, there are at most $2d$ moves in $S'$, each for which one
must choose its type and the variables that compose it. The overall
number of possibilities for $S'$ is
$\mathcal{O}(\binom{N}{3}^{2d})$. Moreover, \cite{lackenby} proved
that $d$ can be bounded by a polynomial in $n$, thus $N$ is polynomial
in $n$ and the algorithm \ref{algo} has overall complexity
$\mathcal{O}(P(n)^{2d})$ where $P$ is a polynomial, this proves that the
problem of untangling a diagram of an unknot with minimal \Rm sequence
knowing the defect belongs to the $XP$ class.
