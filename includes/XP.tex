\section{General case}

\begin{definition}
  Let $S$ be a sequence of \Rms. We define the set of \emph{special moves} of
  $S$ inductively :
  \begin{itemize}
  \item Every move $m$ of type other than $II^-$ is special.
  \item Every type $II^-$ \Rm $\{a, b\}_{II^-}$ such that $a$ or $b$
    is involved in a special move is a special move. More precisely,
    if $a$ (or $b$) verifies one of the following conditions:
    \begin{itemize}
    \item $a$ (or $b$) is a crossing added by a $I^+$ or $II^+$ move of $S$,
    \item $a$ (or $b$) is the endpoint of an arc on which a $I^+$ or $II^+$ move
      is performed,
    \item $a$ (or $b$) is involved in a type $III$ \Rm,
    \end{itemize}
    then $\{a, b\}_{II^-}$ is a special move.
  \end{itemize}
  We call the subsequence of special moves of a sequence $S$ the
  \emph{special subsequence of $S$} and the crossings involved in the
  special moves the \emph{special crossings}.

  The non-special crossings are thus affected by at most one move (of
  type $II^-$).
\end{definition}

%% \begin{lem}
%%  Let $D$ be a diagram of an unknot. Let $S_1$ and $S_2$ be two
%%   sequences having the same special subsequence and removing the same
%%   set of crossings. Then the diagramms obtained from $D$ after $S_1$
%%   and $S_2$ are planar isotopic.
%%   \begin{proof}
%%     \begin{figure}[h]
%%       \begin{subfigure}[b]{0.23\textwidth}
%%         \centering
%%         \includegraphics[width=\textwidth]{figures/pdf/samediag1.pdf}
%%         \caption{Connections between the strands of $c_i$}
%%         \label{fig:samediag1}
%%       \end{subfigure}
%%       \hfill
%%       \begin{subfigure}[b]{0.23\textwidth}
%%         \centering  
%%         \includegraphics[width=\textwidth]{figures/pdf/samediag2.pdf}
%%         \caption{Connection that could not happen in a knot}
%%         \label{fig:samediag2}
%%       \end{subfigure}
%%       \hfill
%%       \begin{subfigure}[b]{0.23\textwidth}
%%         \centering  
%%         \includegraphics[width=\textwidth]{figures/pdf/samediag3.pdf}
%%         \caption{Diagram before the type $II^-$ \Rm}
%%         \label{fig:samediag3}
%%       \end{subfigure}
%%       \hfill
%%       \begin{subfigure}[b]{0.23\textwidth}
%%         \centering  
%%         \includegraphics[width=\textwidth]{figures/pdf/samediag4.pdf}
%%         \caption{Diagram after the type $II^-$ \Rm}
%%         \label{fig:samediag4}
%%       \end{subfigure}

%%       \caption{Outline of the local changes performed during a type
%%         $II^-$ \Rm}
%%       \label{fig:samediag}
%%     \end{figure}
    
%%     One may notice that for each crossing $c_i$ removed by a type
%%     $II^-$ \Rm, the four strands starting in $c_i$ are connected two
%%     by two (see figure \ref{fig:samediag1}) and that the strands can
%%     be only connected with their neighbours and not with the opposite
%%     strand, otherwise, the diagram would depict a link with more than
%%     one component (see figure \ref{fig:samediag2}). Thus, removing
%%     $c_i$ and $c_j$ with a type $II^-$ \Rm, is equivalent to change
%%     the neighbourhood of both crossings as depicted on figures
%%     \ref{fig:samediag3} and \ref{fig:samediag4}. Since the crossings
%%     are removed two by two and the strands of each crossings are
%%     connected to one another in a certain manner, the type $II^-$ \Rm
%%     affecting $c_i$ changes its neighbourhood in the same way whatever
%%     crossing $c_j$ is removed at the same time.

%%     Likewise, a type $II^+$ has only a local effect on two small
%%     sections of two arcs.
%%   \end{proof}
%% \end{lem}

%% \begin{lem}
%%   \label{commute}
%%   Let $D$ be a diagramm in which an arbitrary move $m_1$ (either
%%   special or of type $II^-$) is feasible and a type $II^-$ \Rm $m_2 =
%%   \{c_1, c_2\}_{II^-}$ (regarding other crossings) is feasible as
%%   well. Both sequences $(m_1, m_2)$ and $(m_2, m_1)$ are feasible and
%%   lead to the same diagram.
%%   \begin{proof}
%%     The \Rm $m_1$ can be of type
%%     \begin{description}
%%     \item[$I^-$]
%%     \item[$I^+$]
%%     \item[$II^-$]
%%     \item[$II^+$]
%%     \item[$III$]
%%     \end{description}
%%   \end{proof}
%% \end{lem}
    
\begin{thm}
  Let $D$ be a diagram of an unknot that can be untangled with a
  sequence $S = (\{c_1, c_2\}_{II^-}, \dots \{c_{2k-1},
  c_{2k}\}_{II^-}, m_1, \{c_{2k+1}, c_{2k+2}\}_{II^-} \dots m_2
  \dots)$, where $c_1, \dots c_{2N}$ are the non-special crossings and
  $m_1, \dots m_n$ are the special moves.

  Let $\{c_i, c_j\}_{II^-}$ be a feasible type $II^-$ \Rm in $D$. It
  is possible to untangled $D$ with a sequence of \Rms having the same
  exact special subsequence and starting with $\{c_i, c_j\}_{II^-}$.
  \begin{proof}
    We can assume without loss of generality that $i<j$, and that $i$
    is even while $j$ is odd (by relabeling the two crossings involved
    in the type $II^-$ \Rm removing $c_i$, respectively $c_j$, which
    does not change the move).
    \clearpage
    There are two possible cases:
    \begin{itemize}
    \item $\{c_i, c_j\}_{II^-}$ is a move of $S$. We have $c_j =
      c_{i+1}$.

      \noindent\fbox{\parbox{\linewidth-2\fboxrule-2\fboxsep}{$S$ has
          the form $(\underbrace{\{c_1, c_2\}_{II^-}, \dots}_{S_1}
          \{c_{i}, c_{i+1}\}_{II^-}, \underbrace{\dots}_{S_2})$. One
          may notice that the subsequences $S_1$ and $S_2$ may contain
          special moves. We will show that the sequence $(\{c_i,
          c_{i+1}\}_{II^-}, S_1, S_2)$ is a feasible sequence in
          $D$. To do that, we recursively switch the \Rm $\{c_i,
          c_{i+1}\}_{II^-}$ with its predecessor $p$ in $S$, to pull
          $\{c_i,c_{i+1}\}_{II^-}$ back at the beginning of $S$.}}

      The crossings $c_i$ and $c_{i+1}$ are not affected by $p$,
      because $\{c_i, c_j\}_{II^-}$ is not a special \Rm.

      Let $D'$ be the diagram obtained from $D$ in the course of $S$,
      just before $p$ is performed.

      The \Rm $\{c_i, c_{i+1}\}_{II^-}$ is feasible in
      $D$, thus $c_i$ and $c_j$ are 0-close $D$ with the respect to
      all the other crossings of $D$. Likewise, the second condition
      of Lemma \ref{feasible2-} is verified in $D$. Lemma
      \ref{resistance2-} shows that these conditions are still
      verified in $D'$. In order to know if $\{c_i, c_{i+1}\}_{II^-}$
      is feasible in $D'$, we need to show that $c_i$ and $c_j$ are
      0-close not only in respect to the crossings of $D$ that were
      not deleted, but also if we consider the crossings introduced by
      additive moves as well. If a crossing was introduced in $\alpha$
      or $\beta$ this would mean that there was a $I^+$ or $II^+$ move using
      $\alpha$ or $\beta$ which is impossible since $c_i$ and $c_j$
      are not special crossings. In the end, $\{c_i, c_j\}_{II^-}$ is
      feasible in $D'$.

      \begin{itemize}
      \item $p$ is of type $II^-$: $p = \{a, b\}_{II^-}$.\\ We can
        start by performing $\{c_i, c_{i+1}\}_{II^-}$, let $D''$ be
        the diagram obtained. Since $\{a, b\}_{II^-}$ was feasible in
        $D'$, $a$ and $b$ are 0-close in $D'$ with respect to the set
        of remaining crossings $C$ (Lemma \ref{feasible2-}). Then the
        Lemma \ref{resistance2-} shows that $a$ and $b$ are 0-close in
        $D''$ with respect to $C\setminus \{c_i, c_{i+1}\}$ which is
        exactly the set of remaining crossings of $D''$. Likewise, the
        second condition of Lemma \ref{feasible2-} is verified in $D'$
        and in $D''$ (Lemma \ref{resistance2-}), thus $\{a,
        b\}_{II^-}$ remains feasible in $D''$ (Lemma
        \ref{feasible2-}). To sum up, both sequences $(\{a,
        b\}_{II^-}, \{c_i, c_{i+1}\}_{II^-})$ and $(\{c_i,
        c_{i+1}\}_{II^-}, \{a, b\}_{II^-})$ can be performed in
        $D'$.

        Furthermore, they lead to the same diagram thanks to
        Proposition \ref{samediag}.
        
      \item $p$ is of type $I^-$: $p = \{a\}_{I^-}$.\\ We can start by
        performing $\{c_i, c_{i+1}\}_{II^-}$, let $D''$ be the diagram
        obtained. Since $\{a\}_{I^-}$ was feasible in $D'$, $a$ is
        0-self-close in $D'$ with respect to the set of remaining
        crossings $C$ (Lemma \ref{feasible1-}). Then Lemma
        \ref{resistance1-} shows that $a$ is 0-self-close in $D''$
        with respect to $C\setminus \{c_i, c_{i+1}\}$ which is exactly the
        set of remaining crossings of $D''$. Likewise, the second
        condition of Lemma \ref{feasible1-} is verified in $D'$ and in
        $D''$ (Lemma \ref{resistance1-}), thus $\{a\}_{I^-}$ remains
        feasible in $D''$. To sum up, both sequences $(\{a\}_{I^-},
        \{c_i, c_{i+1}\}_{II^-})$ and $(\{c_i, c_{i+1}\}_{II^-},
        \{a\}_{I^-})$ can be performed in $D'$.

        In both sequences the move $\{c_i, c_{i+1}\}_{II^-}$ affects
        the diagram identically and locally, only in the neighborhood
        of the crossings $c_i$ and $c_j$ (Proposition \ref{samediag}). In
        both sequences $\{a\}_{I-}$ affect only the neighborhood $a$
        by removing the loop around $a$ and this loops does not
        intersect any other crossings, thus $\{a\}_{I^-}$ has also a
        local effect and both sequences lead to the same diagram.
        
      \item $p$ is of type $III$: $p = \{a,b,c\}_{III}$.
        
        We can start by performing $\{c_i, c_{i+1}\}_{II^-}$, let
        $D''$ be the diagram obtained. Since $\{a, b,c\}_{III}$ was
        feasible in $D'$, $(a, b, c)$ is 0-close in $D'$ with respect
        to the set of remaining crossings $C$ (Lemma
        \ref{feasible3}). Then Lemma \ref{resistance3} shows that
        $(a, b, c)$ is 0-close in $D''$ with respect to the
        $C\setminus \{c_i, c_{i+1}\}$ which is exactly the set of
        remaining crossings of $D'$. Likewise, the second condition of
        Lemma \ref{feasible3} is verified in $D'$ and in $D''$ (Lemma
        \ref{resistance3}), thus $\{a, b, c\}_{III}$ remains feasible
        in $D''$. To sum up, both sequences $(\{a, b, c\}_{III},
        \{c_i, c_{i+1}\}_{II^-})$ and $(\{c_i, c_{i+1}\}_{II^-}, \{a,
        b, c\}_{III})$ can be performed in $D'$.

        In both sequences, the move $\{c_i, c_{i+1}\}_{II^-}$ affects
        the diagrams identically and locally, only in the neighborhood
        of the crossings $c_i$ and $c_j$ (Proposition \ref{samediag}). In
        both sequences, no crossing lies in the triangle $(a, b, c)$
        before performing the move $\{a, b, c\}_{III}$ and neither
        $c_i$ nor $c_{i+1}$ is equal to $a$, $b$ or $c$ since $a$, $b$
        and $c$ are special crossings whereas $c_i$ and $c_j$ are
        not. Thus, in both sequences, $\{a, b, c\}_{III}$ has only a
        local effect on the triangle $(a, b, c)$ and not on the rest
        of the diagram. In the end, both sequences lead to the same
        diagram.
        
      \item $p$ is of type $I^+$: $p = \{(ab), w\}_{I^+}$.

        We can start by performing $\{c_i, c_{i+1}\}_{II^-}$ and Lemma
        \ref{feasible1+} shows that in the new diagram obtained,
        $\{(ab), w\}_{I^+}$ remains feasible and can be applied as
        well. To sum up, both sequences $(\{(ab), w\}_{II^-}, \{c_i,
        c_{i+1}\}_{II^-})$ and $(\{c_i, c_{i+1}\}_{II^-}, \{(ab),
        w\}_{II^-})$ can be performed in $D'$.

        In both sequences, the move $\{c_i, c_{i+1}\}_{II^-}$ affects
        the diagrams identically and locally, only in the neighborhood
        of the crossings $c_i$ and $c_j$ (Lemma \ref{fig:samediag3}).
        In both sequences, the move $\{(ab), w\}_{I^+}$ only affects
        the arc $(ab)$ and neither $c_i$ nor $c_{i+1}$ is equal to $a$
        or $b$ since $a$ and $b$ are special crossings whereas $c_i$
        and $c_j$ are not. Thus, in both sequences, $\{(ab),
        w\}_{I^+}$ has only a local effect on the arc $(ab)$ and not
        on the rest of the diagram. In the end, both sequences lead to
        the same diagram.
        
      \item $p$ is of type $II^+$: $p = \{(ab),(cd), \mathcal{S},
        \mathcal{A}\}_{II^+}$.

        We can start by performing $\{c_i, c_{i+1}\}_{II^-}$ and since
        $c_i$ and $c_j$ are non-special crossings, they do not belong
        to $\{a, b, c, d\}$ and Lemma \ref{feasible2+} shows that in
        the new diagram obtained, $\{(ab), (cd), \mathcal{S},
        \mathcal{A}\}_{I^+}$ remains feasible and can be applied as
        well. To sum up, both sequences $(\{(ab), (cd) ,\mathcal{S},
        \mathcal{A}\}_{II^+}, \{c_i, c_{i+1}\}_{II^-})$ and $(\{c_i,
        c_{i+1}\}_{II^-}, \{(ab), (cd), w, \mathcal{A}\}_{II^+})$ can
        be performed in $D'$.

        In both sequences, the move $\{c_i, c_{i+1}\}_{II^-}$ affects
        the diagrams identically and locally, only in the neighborhood
        of the crossings $c_i$ and $c_j$ (Proposition \ref{samediag}). In
        both sequences, $\{(ab), (cd), \mathcal{S},
        \mathcal{A}\}_{II^+}$ only affects the arcs $(ab)$ and $(cd)$,
        and not the rest of the diagram. In the end, both sequences
        lead to the same diagram.
      \end{itemize}
      

    \item $c_i$ and $c_j$ are removed by to different moves in
      $S$. Since we assumed that $i$ is even (respectively that $j$ is
      odd), $c_i$ (respectively $c_j$) is removed in $S$
      by the move $\{c_i, c_{i+1}\}_{II^-}$ (respectively by
      $\{c_{j-1}, c_j\}_{II^-}$).

      \noindent\fbox{\parbox{\linewidth-2\fboxrule-2\fboxsep}{$S$ has
          the form $(\underbrace{\{c_1, c_2\}_{II^-}, \dots}_{S_1}
          \{c_{i}, c_{i+1}\}_{II^-}, \underbrace{\dots}_{S_2}
          \{c_{j-1},c_{j}\}_{II^-}, \underbrace{\dots}_{S_3})$. One
          may notice that the subsequences $S_1$, $S_2$ and $S_3$ may
          contain special moves. We will prove that the sequence
          $(\{c_i, c_j\}_{II^-}, S_1, S_2, \{c_{i+1},
          c_{j-1}\}_{II^-}, S_3)$ is also feasible and untangles
          $D$. In order to do that, we will first show that the
          sequence $(S_1, S_2, \{c_i, c_{i+1}\}_{II^-}, \{c_{j-1},
          c_{j}\}_{II^-}, S_3)$ is feasible as well and also untangles
          the knot, by recursively switching $\{c_i, c_{i+1}\}_{II^-}$
          with its successor $p$. We will then show that the sequence
          $(S_1, S_2, \{c_i, c_{j}\}_{II^-}, \{c_{i+1},
          c_{j-1}\}_{II^-}, S_3)$ is also feasible and untangles the
          knot. Then the first case of this proof can be applied to
          conclude.}}

      We want first to prove that the sequence $(S_1, S_2, \{c_i,
      c_{i+1}\}_{II^-}, \{c_{j-1}, c_{j}\}_{II^-}, S_3)$ is also
      feasible and untangles the knot, by recursively switching
      $\{c_i, c_{i+1}\}_{II^-}$ with its successor $p$.

      Let $D'$ be the diagram obtained from $D$ in the course of $S$,
      just before $\{c_i, c_{i+1}\}_{II^-}$ is performed. The same
      reasonning as in the previous case shows that $\{c_i,
      c_j\}_{II^-}$ is feasible in $D'$
      \begin{itemize}
      \item $p$ is of type $II^-$: $p = \{a, b\}_{II^-}$.\\ Let $D''$
        be the diagram obtained from $D'$ after $\{c_i,
        c_{i+1}\}_{II^-}$. $p$ is feasible in $D''$, thus, $a$ and $b$
        are 0-close with respect to the set of remaining crossings $C$
        of $D''$. Thanks to Lemma \ref{resistance2-}, $a$ and $b$ are
        also 0-close in $D'$ with respect to $C$. Likewise, the second
        condition of Lemma \ref{feasible2-} is verified in $D''$ and
        in $D'$ (Lemma \ref{resistance2-}).

        To prove that $p$ is feasible in $D'$, we only need to prove
        that $a$ and $b$ are 0-close in regard to $C \cup \{c_i,
        c_{i+1}\}$ in $D'$. The arcs $\alpha$ and $\beta$ used for the
        0-closeness of $a$ and $b$ in $D'$ do not contain $c_j$, and
        since $\{c_i, c_j\}_{II^-}$ is feasible in $D'$, the crossings
        $c_j$ and $c_i$ just next to one another and $c_i$ do not
        belong to the interior or $\alpha$ or $\beta$
        either. Likewise, $\{c_i, c_{i+1}\}_{II^-}$ is feasible in
        $D'$ so $c_{i+1}$ does not belong to the interior of $\alpha$
        or $\beta$.

        We just proved that we can start by performing $p$, let $D'''$
        be the diagram obtained. $\{c_i, c_{i+1}\}_{II^-}$ remains
        feasible in $D'''$ thanks to Lemma \ref{resistance2-}. To sum
        up, both sequences $(\{a, b\}_{II^-}, \{c_i,
        c_{i+1}\}_{II^-})$ and $(\{c_i, c_{i+1}\}_{II^-}, \{a,
        b\}_{II^-})$ can be performed in $D'$. Furthermore, they lead
        to the same diagram thanks to Proposition \ref{samediag}.
        
      \item $p$ is of type $I^-$: $p = \{a\}_{I^-}$.\\ Let $D''$ be
        the diagram obtained from $D'$ after $\{c_i,
        c_{i+1}\}_{II^-}$. $p$ is feasible in $D''$, thus, $a$ is
        0-self-close with respect to the set of remaining crossings
        $C$ and verify the second condition of Lemma
        \ref{feasible1-}. Thanks to Lemma \ref{resistance1-}, this is
        also the case in $D'$. To prove that $p$ is feasible in $D'$,
        we only need to prove that $a$ is 0-self-close with respect to
        $C \cup \{c_i, c_{i+1}\}$ in $D'$. The arc $\alpha$ used for
        the 0-self-closeness of $a$ in $D'$ do not contain $c_j$ and
        since $\{c_i, c_j\}_{II^-}$ is feasible in $D'$, $c_j$ and
        $c_i$ are just next to one another and $c_i$ does not belong
        to $\alpha$ either. We prove similarly that $c_{i+1}$ does not
        belong to $\alpha$ by considering the feasible move $\{c_i,
        c_{i+1}\}_{II^-}$ in $D'$.

        We just proved that we can start by performing $p$, let $D'''$
        be the diagram obtained. $\{c_i, c_{i+1}\}_{II^-}$ remains
        feasible in $D'''$ thanks to Lemma \ref{resistance2-}. To sum
        up, both sequences $(\{a\}_{I^-}, \{c_i, c_{i+1}\}_{II^-})$
        and $(\{c_i, c_{i+1}\}_{II^-}, \{a\}_{I^-})$ can be performed
        in $D'$.

        The same reasonning as the one used in the firts case can be
        applied to show that the obtained diagrams are identical.
        
      \item $p$ is of type $III$: $p = \{a, b, c\}_{III}$.

        Let $D''$ be the diagram obtained from $D'$ after $\{c_i,
        c_{i+1}\}_{II^-}$. $p$ is feasible in $D''$, thus, $(a, b, c)$
        is 0-close with respect to the set of remaining crossings $C$
        of $D''$ and verify the second condition of Lemma
        \ref{feasible3}. Thanks to Lemma \ref{resistance3}, this is
        also the case in $D'$. To prove that $p$ is feasible in $D'$,
        we only need to prove that $(a, b, c)$ is 0-close with respect
        to $C \cup \{c_i, c_{i+1}\}$ in $D'$. The arcs $\alpha$,
        $\beta$ and $\gamma$ used for the 0-self-closeness of $(a, b,
        c)$ in $D'$ do not contain $c_j$ and since $\{c_i,
        c_j\}_{II^-}$ is feasible in $D'$, $c_j$ and $c_i$ are just
        next to one another and $c_i$ does not belong to $\alpha$,
        $\beta$ or $\gamma$ either. We prove similarly that $c_{i+1}$
        does not belong to $\alpha$, $\beta$ or $\gamma$ by
        considering the feasible move $\{c_i, c_{i+1}\}_{II^-}$ in
        $D'$.

        We just proved that we can start by performing $p$, let $D'''$
        be the diagram obtained. $\{c_i, c_{i+1}\}_{II^-}$ remains
        feasible in $D'''$ thanks to Lemma \ref{resistance2-}. To sum
        up, both sequences $(\{a, b, c\}_{III}, \{c_i, c_{i+1}\}_{II^-})$
        and $(\{c_i, c_{i+1}\}_{II^-}, \{a, b, c\}_{III})$ can be performed
        in $D'$. Furthermore, they lead to the same diagram by
        applying the same reasonning as in the first case.
        
      \item $p$ is of type $I^+$: $p = \{(ab), w\}_{I^+}$.

        We can start by performing $p$ since type $I^+$ \Rms are
        always feasible (Lemma \ref{feasible1+}), let $D''$ be the
        diagram obtained. $\{c_i, c_{i+1}\}_{II^-}$ was feasible in
        $D'$, so $c_i$ and $c_{i+1}$ are 0-close in $D'$ with respect
        to the set of remaining crossings $C$. Lemma
        \ref{resistance2-}, shows that it is also the case in $D''$,
        with respect to $C$. Likewise, the second condition of Lemma
        \ref{feasible2-} is verified in $D'$ and in $D''$ (Lemma
        \ref{resistance2-}). To prove that $\{c_i, c_{i+1}\}_{II^-}$
        is feasible in $D''$, we need to show that they are 0-close in
        regard to $C \cup {c}$ where $c$ is the new crossing
        introduced by $p$. Now, $a$ and $b$ are not on the arcs
        $\alpha$ and $\beta$ used for the 0-closeness of $c_i$ and
        $c_{i+1}$, so $c$ is not in the interior of $\alpha$ and
        $\beta$ either.

        To sum up, both sequences $(\{(ab), w\}_{II^-}, \{c_i,
        c_{i+1}\}_{II^-})$ and $(\{c_i, c_{i+1}\}_{II^-}, \{(ab),
        w\}_{II^-})$ can be performed in $D'$. Furthermore, they lead
        to the same diagram by applying the same reasonning as in the
        first case.
        
        
      \item $p$ is of type $II^+$: $p = \{(ab),(cd), \mathcal{S},
        \mathcal{A}\}_{II^+}$.

        Thanks to Lemma \ref{resistance2+}, we can start by performing
        $p$, let $D''$ be the diagram obtained. $\{c_i,
        c_{i+1}\}_{II^-}$ was feasible in $D'$, so $c_i$ and $c_{i+1}$
        are 0-close in $D'$ with respect to the set of remaining
        crossings $C$. Lemma \ref{resistance2-}, shows that it is
        also the case in $D''$, with respect to $C$. Likewise, the
        second condition of Lemma \ref{feasible2-} is verified in $D'$
        and in $D''$ (Lemma \ref{resistance2-}). To prove that $\{c_i,
        c_{i+1}\}_{II^-}$ is feasible in $D''$, we need to show that
        they are 0-close with respect to $C \cup \{e, f\}$ where $e$
        and $f$ are the new crossings introduced by $p$. Now, $a$,
        $b$, $c$ and $d$ are not on the arcs $\alpha$ and $\beta$ used for the
        0-closeness of $c_i$ and $c_{i+1}$, so $e$ and $f$ are not in the
        interior of $\alpha$ and $\beta$ either.

        To sum up, both sequences $(\{(ab), (cd), \mathcal{S},
        \mathcal{A}\}_{II^+}, \{c_i, c_{i+1}\}_{II^-})$ and $(\{c_i,
        c_{i+1}\}_{II^-}, \{(ab), (cd), \mathcal{S},
        \mathcal{A}\}_{II^+})$ can be performed in $D'$. Furthermore,
        they lead to the same diagram by applying the same reasonning
        as in the first case.

      \end{itemize}

      We just proved that the sequence $(S_1, S_2, \{c_i,
      c_{i+1}\}_{II^-}, \{c_j, c_{j+1}\}_{II^-}, S_3)$ is feasible in
      $D$ and untangles the knot. We now want to prove that the
      sequence $(S_1, S_2, \{c_i, c_{j}\}_{II^-}, \{c_{i+1},
      c_{j-1}\}_{II^-}, S_3)$ is also feasible and untangles the
      knot. Let $D_1$ be the diagram obtained from $D$ after the
      sequence $(S_1, S_2)$. In $D_1$, $\{c_i, c_{i+1}\}_{II^-}$ is
      feasible and $\{c_i, c_j\}_{II^-}$ is feasible as well (as shown
      at the beginning of this case).
      
      Let us denote by $D_2$ the diagram obtained from $D_1$ after the
      move $\{c_i, c_{i+1}\}_{II^-}$. The crossings $c_{j-1}$ and
      $c_{j}$ are 0-close in $D_2$ with respect to the set of
      remaining crossings $C$, thus $c_{j-1}$ and $c_j$ are also
      0-close in respect to $C$ in $D_1$.One may notice that $c_i,
      c_{i+1} \notin C$. The second condition of Lemma
      \ref{feasible2-} is verified in $D_2$ and in $D_1$ (Lemma
      \ref{resistance2-}).

      The only piece of diagram in which $\{c_i,
      c_j\}_{II^-}$ is feasible and $\{c_i, c_{i+1}\}_{II^-}$ is
      feasible as well is depicted in figure \ref{fig:config1}.

      \begin{figure}[h]
        \centering  
        \includegraphics[width=.5\textwidth]{figures/pdf/config1.pdf}
        \caption{Only configuration in which $\{c_i, c_j\}_{II^-}$
          and $\{c_i, c_{i+1}\}_{II^-}$ are feasible}
        \label{fig:config1}
      \end{figure}
      
      If we add $c_{j-1}$ in this representation, there are two
      possible cases:
      \begin{itemize}
      \item $c_j$ and $c_{j+1}$ are 0-close with respect to all the
        other crossings (including $c_i$ and $c_{i+1}$).
      \item $c_j$ and $c_{j+1}$ are 0-close with respect to all the
        other crossings (except $c_i$ and $c_{i+1}$), and $c_i$,
        $c_{i+1}$ are in the interior of $\alpha$ or $\beta$.
      \end{itemize}

      In each cases, the second condition of Lemma \ref{feasible2-} is
      verified. Thus the only possible configuration for the first
      case is represented in figure \ref{fig:config2} and the
      configuration for the second case in figure \ref{fig:config3}.

      \begin{figure}[h]
        \begin{subfigure}[b]{0.55\textwidth}
          \centering  
          \includegraphics[width=\textwidth]{figures/pdf/config2.pdf}
          \caption{Only possible configuration if additionally $\{c_j,
            c_{j-1}\}_{II^-}$ is feasible}
          \label{fig:config2}
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{0.4\textwidth}
          \centering  
          \includegraphics[width=\textwidth]{figures/pdf/config3.pdf}
          \caption{Only possible configuration if additionnaly $c_j$
            and $c_{j-1}$ are 2-close but not 0-close}
          \label{fig:config3}
        \end{subfigure}
        \caption{Only two possible configurations for the crossings
          $c_i$, $c_{i+1}$, $c_{j-1}$ and $c_j$}
        \label{fig:config}
      \end{figure}
      
      In the two configurations, the sequence $(\{c_i, c_j\}_{II^-},
      \{c_{i+1}, c_{j-1}\}_{II^-})$ is feasible and this sequence
      leads as the same diagram as $(\{c_i, c_{i+1}\}_{II^-},
      \{c_{j-1}, c_j\}_{II^-})$ (Proposition \ref{samediag}).

      We just proved that the sequence $S_1, S_2, \{c_i, c_j\}_{II^-},
      \{c_{i+1}, c_{j+1}\}_{II^-}, S_3)$ is feasible. We can now
      conclude by applying the first case of the proof.

    \end{itemize}
  \end{proof}
\end{thm}

\begin{remark}
  This shows that it is possible to compute an untangling of a diagram
  $D$ of the unknot by enumerating all the possible special
  subsequences $(m_1, \dots m_n)$ that have a defect equal to $def(D)$
  and in each case greedily perform type $II^-$ moves on the
  non-special crossings until no such move is possible, then try to
  perform $s_1$ and continue greedily with the remaining crossings and
  $(m_2, \dots m_n)$...
\end{remark}

%% \begin{algorithm}
%% \label{algo1}
%% \caption{Computes the untangling of a knot}
%% \begin{algorithmic} 
%% \REQUIRE $D$ the diagram to untangled, $d$ its defect.
%% \STATE $SpecialMoves \leftarrow ()$
%% \STATE $CurrentDefect \leftarrow 0$
%% \STATE $SpecialCrossingsSet \leftarrow \emptyset$
%% \WHILE{$CurrentDefect < d$}
%% \STATE Choose the crossings involved in each move
%% \ENDWHILE
%% \STATE $\tilde{n} \leftarrow n_{\left\lceil \frac{N}{2} \right\rceil}$ 
%% \STATE $current\_dist \leftarrow \sum_{i=1}^{N} |n_{i} - \overline{n}|$
%% \STATE $dist_{min} \leftarrow current\_dist$
%% \FOR{$k \in \llbracket 1;N \rrbracket$}
%% \STATE $\tilde{n} \leftarrow n_{\left\lceil \frac{N+k}{2} \right\rceil}$ 
%% \STATE $current\_dist \leftarrow \sum_{i = 1}^{k} n_{i} + \sum_{i=
%%   k+1}^{N} |n_{i} - \tilde{n}|$
%% \IF{$current\_dist < dist_{min}$}
%% \STATE $dist_{min} \leftarrow current\_dist$
%% \STATE $M_{min} \leftarrow k$
%% \ENDIF
%% \ENDFOR
%% \RETURN $(dist_{min}, M_{min}, n_{\left\lceil \frac{N + M_{min}}{2} \right\rceil})$ 
%% \end{algorithmic}
%% \end{algorithm}
